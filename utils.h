#ifndef UTILS_H
#define UTILS_H

#define IDXR(i,j,ld) (((i)*(ld))+j)
#define IDXC(i,j,ld) (((j)*(ld))+i)
#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include <cstdlib>
#include <stdarg.h>
#include <cmath>
#include <vector>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>

#ifdef USE_BLAS
#include <cblas.h>
#endif
#define MSG 0
#define INFO 1
#define ERROR 2
typedef double real;
using namespace std;
/* File operations */
FILE* safe_fopen(const char* filename,const char* mode);
long safe_ftell(FILE* f);
void safe_seek(FILE* stream,long offset,int whence);
void safe_fread(void *ptr, size_t size, size_t nmemb, FILE *stream);
char* safe_getline(char* str,int size,FILE* stream);
void safe_fclose(FILE* stream);

/*memory operations*/
void* allocMemory(size_t vsize,int num_elem);
void* reallocMemory(void*, size_t, int num_elem);

/*vector print operations*/
void print_vector_1d(real* vec,int size);
void print_vector_2d(real* vec,int rows,int cols);

/*reading vectors from file*/
//void read_vector_1d(float** vec, int* row,int * col,FILE* f,bool binary);
//void read_vector_2d(float** vec, int* row,int * col,FILE* f,bool binary);
void read_vector_1d(real** vec, int* row,int * col,FILE* f,bool binary);
void read_vector_2d(real** vec, int* row,int * col,FILE* f,bool binary);

/*error reporting operations */
void print_error(const char* frmt,...);
void print_message(const char* frmt,...);
void show_message(const char* frmt, ...);
void set_debug_level(int level);
int get_debug_level();
/* random number generation operations*/
real* rand(size_t m, size_t n);
real* randRange(size_t m,size_t n,real a,real b);
real* randn(size_t m,size_t n);

/*Misc*/
void swap(real** a, real**b);
vector<std::string> &split(const string &s, char delim, vector<std::string> &elems) ;
vector<string> split(const string &s, char delim) ;
string &ltrim(std::string &s);
string &rtrim(std::string &s);
string &trim(std::string &s);

/*blas functions*/
#ifdef USE_BLAS
void cblas_xcopy(float* output,float* input,int size);
void cblas_xcopy(double* output,double* input,int size);
void cblas_xgemv(float* output,int output_size,float* input,int input_size,float* biases,float* weights);
void cblas_xgemv(double* output,int output_size,double* input,int input_size,double* biases,double* weights);
#endif
#endif
