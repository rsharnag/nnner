/*
 * Hash.h
 *
 *  Created on: 17-May-2014
 *      Author: cfilt
 */

#ifndef HASH_H_
#define HASH_H_
#include "utils.h"
#include <string>
#include <cstring>
#define MAX_STRING  300
#include <map>
using namespace std;
class Hash {
private:
	int n_keys; ///< number of keys in hash
	char** keys; ///< values of keys
	map<string,int> hash; ///< hash map of keys to index
public:
	Hash(const char* filename);
	virtual ~Hash();
	int getIndex(const char* str);
	const char* getKey(int index);
	int size();
};

#endif /* HASH_H_ */
