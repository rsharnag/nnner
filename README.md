#Named Entity Recognition System#
##Introduction##
A neural network based named entity recognition system. This code includes a trainer module to  train the network. A testing module to test the tagging on individual files and also server module for the interface to query the model on demand.

##Installation##
This module needs g++ to compile the code.

- Step 1: Installing external dependencies
Code uses CBLAS libraries to optimize the linear algebra functions.
For Ubuntu systems - sudo apt-get install libblas-* should install all the needed Libraries to your system

- Step 2: Building the code
To generate the binaries
    - Go to the directory containing cpp files and execute either of the following commands
    - make (To generate both trainer and server executable)
    - make nnner [generates only trainer]
    - make nnner_server [generates only the server executable]

- Step 3: Modify config file
A sample nnner.cfg has been included with the code. Provide all the details of each file needed to execute the code. Configuration file is self documented.

- Step 4 : Execute the code
Now you should be able to execute the code using following commands
    - ./nnner -cfg <config_file\> -scope <scope\> [train/test]
    - ./nnner_server <config_file\> -scope <scope\> -port <port_number\>