/*
 * Hash.cpp
 *
 *  Created on: 17-May-2014
 *      Author: cfilt
 */

#include "Hash.h"
/**
 * lowercase the word
 * @param x - word
 */
void lowerAll(char* x){
	for(int i=0;i<strlen(x) && i<MAX_STRING;i++){
		x[i]=tolower(x[i]);
	}

}
/**
 * Constructs hash using the file. Creates both forward and backward index
 * @param filename - Name of file to be used to create the hash
 */
Hash::Hash(const char *filename) {
	FILE* f;
	char key[MAX_STRING];
	f = safe_fopen(filename,"rt");
	n_keys=0;
	while(safe_getline(key,MAX_STRING,f)){

		hash[key]=n_keys;
		++n_keys;
	}
	safe_fclose(f);
	//forward map
	keys = (char**) allocMemory(sizeof(char*),n_keys);
	memset(keys,0,sizeof(char*)*n_keys);
	int i=0;
	for(map<string,int>::iterator iter = hash.begin(); iter!=hash.end();++iter){
		//printf("%d\t%s\n",iter->second,iter->first.c_str());
		keys[iter->second] = (char*) allocMemory(sizeof(char),strlen(iter->first.c_str())+1);
		strcpy(keys[iter->second],iter->first.c_str());
		i++;
	}
}
/**
 * Destructor
 */
Hash::~Hash() {
	for(int i=0;i<n_keys;++i)
		free(keys[i]);
	free(keys);
}
/**
 * Returns the index of the key provided
 * @param str - string key
 * @return integer index
 */
int Hash::getIndex(const char* str){
	string key(str);
	if(hash.find(key)!=hash.end())
		return hash[str];
	else
		return -1;

}
/**
 * Returns key given the index
 * @param index - integer index
 * @return key value
 */
const char* Hash::getKey(int index){
	return keys[index];
}
/**
 * Returns size of hash
 * @return size of hash
 */
int Hash::size(){
	return hash.size();
}
