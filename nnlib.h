#ifndef NNLIB_H
#define NNLIB_H
#include <math.h>
#include "Tokenizer.h"
#include "utils.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <cassert>
#include <algorithm>
typedef double real;
using namespace std;
typedef real synapse;
typedef real neuron;


/**
 * Feature encapsulation to hold the properties of
 * the various features used by the neural network.
 */
class Feature{
public:
	/*feature properties*/
	bool used; ///<   whether this feature is used
	real* lookup_table; ///< feature vector for each value of the feature
	int vecSize; ///< size of vector representation for each value of feature
	Hash* featureHash; ///< hash of feature values

	Feature():used(false),lookup_table(NULL),vecSize(0),featureHash(NULL){} ///< Feature class implementation
	/**
	 * Feature class constructor
	 * 	@param fHash - feature value hash
	 * 	@param vecSize - vector representation size
	 */
	Feature(Hash* fHash,int vecSize):featureHash(fHash),vecSize(vecSize){
		used=true;
		lookup_table = randn(featureHash->size(),vecSize);
	}
	virtual ~Feature(){
		free(lookup_table);
	}
	/**
	 * Resets the feature table to one hot representation
	 */
	void reset(){
		if(lookup_table) free(lookup_table);
		lookup_table= (real*) allocMemory(sizeof(real),featureHash->size()*featureHash->size());
		for(int i=0;i<featureHash->size();i++){
			for(int j=0;j<featureHash->size();j++){
				lookup_table[IDXR(i,j,featureHash->size())]= -1.0;
			}
		}
		for(int i=0;i<featureHash->size();i++){
			lookup_table[IDXR(i,i,featureHash->size())]=1.0;
		}
		vecSize = featureHash->size();
	}
	/**
	 * Serializes the Feature in file
	 * @param  f - file pointer
	 */
	void serialize(FILE *f){
		int x = featureHash->size();
		fwrite(&used,sizeof(bool),1,f);
		fwrite(&vecSize,sizeof(int),1,f);
		fwrite(&x,sizeof(int),1,f);
		fwrite(lookup_table,sizeof(real),vecSize*featureHash->size(),f);
	}
	/**
	 * Deserialize the feature from a file
	 * @param f - file pointer
	 */
	void deserialize(FILE*f){
		int y,res;
		vecSize=0;
		if(fread(&used,sizeof(bool),1,f)!=1) print_error("used problem");
		if(fread(&vecSize,sizeof(int),1,f)!=1) print_error("vecsize problem");
		if(fread(&y,sizeof(int),1,f)!=1) print_error("y problem");
		res = fread(lookup_table,sizeof(real),vecSize*y,f);
		if(res !=vecSize*y) print_error("lookup_table problem");
		if(y!=featureHash->size()){
			print_error("Hash not initialized");
		}

	}
};
/**
 * Neural network trainer implementation
 */
class NNNer
{
    public:
        NNNer(const char* wordVecFile,int windowSize,int hiddenStateSize,int outputVecSize,int pad,real lr,real alpha,int max_epoch,bool binary= true,const  char * oPath=NULL);
        virtual ~NNNer();
        void setTrainFile(const char* filename);
        void setTestFile(const char* filename);
        void setValidFile(const char* filename);
        void setParamFile(const char* filename);
        void loadWordVec(const char* filename);
        void storeNNet();
        void restoreNNet();
        void linear_layer(real *output, int output_size,real *weights, real *biases, real *input, int input_size);
        void hardtanh_layer(real *output, real *input, int size);
        void nnLookup(real* dest,int stride,const real* weights,int wordVecSize,
        		int maxidx,const int* wordIndices,int numWords,int padidx,int npad);
        void forwardPass(Tokens& e);
        void learnNet(vector<Tokens>& training_data,vector<Tokens>& validation_data);
		real dtanh(real);
		inline real dsoftmax(real);
		void softmax_layer(real* output,real* input,int input_size);
		void sigmoid_layer(real *output,real *input, int input_size);
		void viterbi_inference(int* path, real* init, real* transition,
				real* emission, int N, int T);
		void test(vector<Tokens>& training_data);
		void test(Tokens& training_data);
		void getEmpiricalTransitionProb(vector<Tokens>& training);
		void updateFeatures(real* lookupTable, int stride, const real* delta,
				int vecSize, int maxidx, const int* featureIndices, int numWords,int padidx, int npad);

		void setCapFeature( Feature*& capFeature){///< sets capitalization feature
			this->capFeature = capFeature;
			input_state_size+=capFeature->vecSize;
		}

		void setGazlFeature( Feature*& gazlFeature) {///< sets location gazetteer feature
			this->gazlFeature = gazlFeature;
			input_state_size+=gazlFeature->vecSize;
		}

		void setGazmFeature( Feature*& gazmFeature) {///< sets misc gazetteer feature
			this->gazmFeature = gazmFeature;
			input_state_size+=gazmFeature->vecSize;
		}

		void setGazoFeature( Feature*& gazoFeature) {///< sets organization gazetteer feature
			this->gazoFeature = gazoFeature;
			input_state_size+=gazoFeature->vecSize;
		}

		void setGazpFeature( Feature*& gazpFeature) {///< sets person gazetteer feature
			this->gazpFeature = gazpFeature;
			input_state_size+=gazpFeature->vecSize;
		}
		void setResIdfFeature( Feature*& rFeature) {///< sets residf feature
					this->resIdfFeature = rFeature;
					input_state_size+=resIdfFeature->vecSize;
		}

	void setBdllFeature( Feature*& bdllFeature) {///< sets bdll feature
		this->bdllFeature = bdllFeature;
		input_state_size+=bdllFeature->vecSize;
	}

	void setDiceFeature( Feature*& diceFeature) {///< sets dice feature
		this->diceFeature = diceFeature;
		input_state_size+=diceFeature->vecSize;
	}

	void setEntropyFeature( Feature*& entropyFeature) {///< sets entropy feature
		this->entropyFeature = entropyFeature;
		input_state_size += entropyFeature->vecSize;
	}

	void setNpmiFeature( Feature*& npmiFeature) {///< sets npmi feature
		this->npmiFeature = npmiFeature;
		input_state_size+=npmiFeature->vecSize;
	}

    protected:
    private:
        char trainFile[300];///< training file name holder
        char validFile[300];///< validation file name holder
        char testFile[300];///< test file name holder
        char paramFile[300]; ///< parameter file name holder
        char outputPath[300];///< output file name holder
        int max_epoch; ///< maximum epochs to perform
        real random(real min,real max);
        void hardtanh(real* output, real* input,int size);
        void initNet();
        real sigmoid(real x);
        real dsigmoid(real x);
        int countWords(vector<Tokens>& t);
        void validate(vector<Tokens>& data,int epoch);
        real updateLearningRate(vector<Tokens>& val_data,real lr);
        int window_size; ///< Window size
        int wordVecSize; ///< Word vector size
        int vocabSize; ///< Max vocabulary size
        int input_state_size; ///< input vector size to network [window_size * wordVecSize]
        int hidden_state_size; ///< hidden state size

        int output_state_size; ///< output state size (number of tags )
        real* word_lookup_table; ///< word vector table
        real *l1_weight; ///< layer 1 weights
        real *l1_bias; ///< layer 1 biases
        real *l2_weight; ///< layer 2 weight
        real *l2_bias; ///< layer 2 biases
        real *prev_l1_weight; ///< prev delta for layer 1 weights
        real *prev_l1_bias; ///<  prev delta for layer 1 biases
        real *prev_l2_weight; ///< prev delta for layer 2 weight
        real *prev_l2_bias; ///< prev delta for layer 2 biases
        real *viterbi_score_init; ///< viterbi initial scores
        real *viterbi_score_trans; ///< viterbi transition scores

        real starting_lr; ///< starting learning rate
        real lr; ///< Running learning rate
        real alpha;///< momentum
        /* states */
        real *input_state;  ///< input state vector
        real *hidden_state; ///< hidden state vector
        real *output_state; ///< output state vector
        int *labels;///< label ids
        real prev_cost; ///< Previous cost during validation

        /* padding indices */
        int word_padding_idx; ///< word padding index


        bool binary;///< Mode for writing file
        /**
         *  features object. Generalization is needed in future implementation
         */
        Feature* capFeature; ///< Capitalization feature object
        Feature* gazpFeature;///< person gazetteer feature object
        Feature* gazoFeature;///< organization gazetteer feature object
        Feature* gazlFeature;///< location gazetteer feature object
        Feature* gazmFeature;///< misc gazetteer feature object
        Feature* bdllFeature;///< bidirectional loglikelihood feature object
        Feature* entropyFeature;///< entropy feature object
        Feature* diceFeature;///< DICE feature object
        Feature* npmiFeature;///< normalized PMI feature object
        Feature* resIdfFeature;///< Residual IDF feature object



};

#endif // NNLIB_H
