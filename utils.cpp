#include "utils.h"
#include <iostream>
static int DEBUG_LEVEL = MSG;
using namespace std;
static void buffer_reverse_memory(void *ptr_, int block_size, int n_blocks);
static int is_little_endian_cpu();
/**
 * prints error message and terminates the process
 * @param frmt - format of error message
 * @param ... substitution argument in the format
 *
 */
void print_error(const char * frmt, ...) {
	va_list args;

	fprintf(stderr, "FATAL ERROR:");
	va_start(args, frmt);
	vfprintf(stderr, frmt, args);
	fprintf(stderr, "\n\n");
	va_end(args);
	exit(1);
}
/**
 * return current debug level
 */
int get_debug_level() {
	return DEBUG_LEVEL;
}
/**
 * prints error message to the console
 * @param frmt - format of error message
 * @param ... substitution argument in the format
 */
void print_message(const char* frmt, ...) {
	va_list args;
	if (DEBUG_LEVEL <= INFO) {
		fprintf(stderr, "INFO:");
		va_start(args, frmt);
		vfprintf(stderr, frmt, args);
		fprintf(stderr, "\n\n");
		va_end(args);
	}
}
/**
 * Opens a file and also performs error checking
 * @param filename - path of the file
 * @param mode - mode of file opening
 * @return FILE pointer to the opened file
 */
FILE* safe_fopen(const char* filename, const char* mode) {
	FILE* f;
	f = fopen(filename, mode);
	if (!f)
		print_error("Unable to load file %s", filename);
	return f;
}
/**
 * Returns current position in the file
 * @param f - File pointer
 * @return position in the file
 */
long safe_ftell(FILE* f) {
	long t = ftell(f);
	if (t == -1)
		print_error("Unable to tell where i am in the file");
	return t;

}
/**
 * Error parsed function to seek to a particular position in the file
 * @param stream - File pointer
 * @param offset - number of bytes to skip from origin
 * @param whence - position used as reference for the offset
 */
void safe_seek(FILE* stream, long offset, int whence) {
	if (fseek(stream, offset, whence)) {
		print_error("Unable to seek to this position in file (%ld,%d)", offset,
				whence);
	}
}
/**
 * Reads an array of \p nmemb elements, each one with a size of \p size bytes,
 * from the \p stream and stores them in the block of memory specified by \p ptr.
 * This function handles big endian and little endian architecture
 * @param ptr - ptr to store the read
 * @param size - Size,in bytes, of each element to be read
 * @param nmemb -  number of elements to be read
 * @param stream - FILE pointer that specifies input file
 */
void safe_fread(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t res = fread(ptr, size, nmemb, stream);

	if (res != nmemb)
		print_error(
				"read error: read %ld elements instead of %ld (of size %ld)",
				res, nmemb, size);

	if (size > 1 && !is_little_endian_cpu())
		buffer_reverse_memory(ptr, size, nmemb);
}
/**
 * Wrapper around fgets function to safely read a line
 * @param str - buffer holder for read characters from stream
 * @param size - size of str(buffer)
 * @param stream - File pointer
 * @return pointer to populated string
 */
char* safe_getline(char* str, int size, FILE* stream) {
	int str_size;
	if (fgets(str, size, stream)) {
		str_size = strlen(str);
		if (str_size > 0 && str[str_size - 1] == '\n')
			str[str_size - 1] = '\0';
		return str;
	} else {
		return NULL;
	}
}
/**
 * Close the stream
 * @param stream - File pointer
 */
void safe_fclose(FILE* stream) {
	fclose(stream);
}
/**
 * Displays messages to standard output rather than stderr
 */
void show_message(const char* frmt, ...) {
	va_list args;
	fprintf(stdout, "INFO:");
	va_start(args, frmt);
	vfprintf(stdout, frmt, args);
	va_end(args);
}
/**
 * Safe allocation of memory all initialized to 0
 * @param vsize - size of each element
 * @param num_elem - number of elements to be allocated
 * @return pointer to the allocated memory
 */
void* allocMemory(size_t vsize, int num_elem) {
	void* ret = calloc(num_elem , vsize);
	if (!ret) {
		print_error("Could not allocate memory. Buy some more RAM");
	}
	return ret;
}
/**
 * Wrapper over realloc memory that performs error checking.
 * @param abc - pointer to be reallocated
 * @param elem_size - size of each element
 * @param num_elem - number of elements in new blocks
 * @return pointer to reallocated memory block
 */
void* reallocMemory(void* abc, size_t elem_size, int num_elem) {
	void* ret = realloc(abc, elem_size * num_elem);
	if (!ret) {
		print_error("Could not reallocate memory. Buy some more RAM");
	}
	return ret;
}
/**
 * Used for conversion from big endian architecture to little endian architecture
 */
static void buffer_reverse_memory(void *ptr_, int block_size, int n_blocks) {
	char *ptr;
	char *ptrr;
	char *ptrw;
	int i, j;
	char *buffer_block;

	if (block_size == 1)
		return;

	ptr = (char *) ptr_;
	buffer_block =(char*) allocMemory(sizeof(char), block_size);

	for (i = 0; i < n_blocks; i++) {
		ptrr = ptr + ((i + 1) * block_size);
		ptrw = buffer_block;

		for (j = 0; j < block_size; j++) {
			ptrr--;
			*ptrw++ = *ptrr;
		}

		ptrr = buffer_block;
		ptrw = ptr + (i * block_size);
		for (j = 0; j < block_size; j++)
			*ptrw++ = *ptrr++;
	}

	free(buffer_block);
}
/**
 * Prints 1-dimensional vector
 * @param vec - pointer to vector
 * @param size - size of vector
 */
void print_vector_1d(real* vec, int size) {
	if (DEBUG_LEVEL >= INFO)
		return;
	printf("Size of vector:%d\nVector:", size);
	for (int i = 0; i < size; ++i) {
		printf("%6.4f ", vec[i]);
	}
	printf("\n\n");
	fflush(stdout);
}
//void print_vector_1d(double* vec, int size) {
//	if (DEBUG_LEVEL >= INFO)
//		return;
//	printf("Size of vector:%d\nVector:", size);
//	for (int i = 0; i < size; ++i) {
//		printf("%6.4lf ", vec[i]);
//	}
//	printf("\n\n");
//	fflush(stdout);
//}
/**
 * Prints 2-dimensional vector
 * @param mat - pointer to matrix m
 * @param rows - number of rows in matrix mat
 * @param cols - number of cols in matrix mat
 * \remark Assumes row major format for storage
 */
void print_vector_2d(real* mat, int rows, int cols) {
	if (DEBUG_LEVEL >= INFO)
		return;
	printf("Size of matrix: %dx%d\nMatrix:", rows, cols);
	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			printf("%6.4f ", mat[IDXR(i,j,cols)]);
		}
		printf("\n");
	}
	printf("\n\n");
	fflush(stdout);
}
//void print_vector_2d(double* mat, int rows, int cols) {
//	if (DEBUG_LEVEL >= INFO)
//		return;
//	printf("Size of matrix: %dx%d\nMatrix:", rows, cols);
//	for (int i = 0; i < rows; ++i) {
//		for (int j = 0; j < cols; ++j) {
//			printf("%6.4lf", mat[IDXC(i,j,rows)]);
//		}
//		printf("\n");
//	}
//	printf("\n\n");
//	fflush(stdout);
//}
/**
 * Function to set the debug level in the code
 * @param level - Debug level
 */
void set_debug_level(int level) {
	DEBUG_LEVEL = level;
}
/**
 * Generates a matrix of size m x n with values initialized with uniform random numbers
 * @param m - number of rows in the matrix
 * @param n - number of columns in the matrix
 * @return pointer  to m x n matrix
 */
real* rand(size_t m, size_t n) {
	real* x = (real*) malloc(sizeof(real) * m * n);
	size_t i, j;
	real rand_max = real(RAND_MAX);
//	std::srand (time(NULL));
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++)
			x[n * i + j] = double(std::rand()) / rand_max;
	}
	return x;
}
/**
 * Generates a matrix of size m x n with values initialized with uniform random numbers in the range of a and b
 * @param m - number of rows in the matrix
 * @param n - number of columns in the matrix
 * @param a - min value
 * @param b - max value
 * @return pointer  to m x n matrix
 */
real* randRange(size_t m, size_t n, real a, real b) {
	real* x = rand(m, n);
	for (int i = 0; i < m * n; i++) {
		x[i] = (b - a) * x[i] + a;
	}
	return x;
}
/**
 * Generates a matrix of size m x n with values initialized with  numbers from a normal distribution
 * @param m - number of rows in the matrix
 * @param n - number of columns in the matrix
 * @return pointer  to m x n matrix
 */
real* randn(size_t m, size_t n) {
	// use formula 30.3 of Statistical Distributions (3rd ed)
	// Merran Evans, Nicholas Hastings, and Brian Peacock
	// Code borrowed from Octave. Need column major format for array
	real* u = rand(m * n + 1, 1);
	real* x = (real*) allocMemory(sizeof(real) , m * n);
	size_t i, j, k;
	real pi = 4. * std::atan(1.);
	real square, amp, angle;
	k = 0;
	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			if (k % 2 == 0) {
				square = -2. * std::log(u[IDXC(k,0,1)]);
				if (square < 0.)
					square = 0.;
				amp = sqrt(square);
				angle = 2. * pi * u[IDXC((k + 1), 0,1)];
				x[IDXC(i,j,m)] = amp * std::sin(angle);
			} else
				x[IDXC(i,j,m)] = amp * std::cos(angle);
			k++;
		}
	}
	free(u);
	return x;
}
/**
 * Utility function to swap two real numbers
 */
void swap(real** a, real** b) {
	real* temp=*a;
	*a = *b;
	*b = temp;
}
/**
 * split function to split a string on the delim character
 * @param s - string to be split
 * @param delim - delimiter to be used for splitting
 * @param elems - vector to hold the segments after the split
 * @return reference to the vector of splits
 */
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    cout<<elems[0];
    return elems;
}

/**
 * split function to split a string on the delim character
 * @param s - string to be split
 * @param delim - delimiter to be used for splitting
 * @return vector of splits
 */
std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}
// trim from start
string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
 string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
 string &trim(std::string &s) {
        return ltrim(rtrim(s));
}
/**
 * check for little endian architecture
 * @return boolean
 */
static int is_little_endian_cpu() {
	int x = 7;
	char *ptr = (char *) &x;

	if (ptr[0] == 0)
		return 0;
	else
		return 1;
}
/**
 * Safe read of 2 dimensional matrix stored from a file
 * @param vec - output vector to store the matrix
 * @param row - number of rows in matrix
 * @param col - number of col in matrix
 * @param f - file pointer
 * @param binary - make read in binary or ascii format
 * \remark - assumes row major format for the matrix
 */
void read_vector_2d(real** vec, int* row, int* col, FILE* f, bool binary) {
	if (binary) {
		safe_fread(row, sizeof(int), 1, f);
		safe_fread(col, sizeof(int), 1, f);
	} else {
		if(fscanf(f, "%d %d\n", row, col)!=2) print_error("Problem reading from fscanf");;
	}
	(*vec) = (real*) allocMemory(sizeof(real), (*row) * (*col));
	if (binary) {
		safe_fread(*vec, sizeof(real), ((long) *row) * (*col), f);
	} else {
		for (int i = 0; i < *row; ++i) {
			for (int j = 0; j < *col; ++j) {
				if (sizeof(real) == 4) { //real is currently float
					if(fscanf(f, "%f", (float*)&((*vec)[IDXR(i,j,*col)]))!=1) print_error("Problem reading from fscanf");;
				} else {
					if(fscanf(f, "%lf", &((*vec)[IDXR(i,j,*col)]))!=1) print_error("Problem reading from fscanf");;
				}
			}
		}
	}
}
/**
 * Safe read of 1 dimensional vector from a file
 * @param vec - vector to store the vector
 * @param vecSize - number of elements in the vector
 * @param f - file pointer
 * @param binary - make read in binary or ascii format
 */
void read_vector_1d(real** vec, int* vecSize, FILE* f, bool binary) {
	if (binary) {
		safe_fread(vecSize, sizeof(int), 1, f);
	} else {
		if(fscanf(f, "%d", vecSize)!=1) print_error("Problem reading from fscanf");
	}
	(*vec) = (real*) allocMemory(sizeof(real), *vecSize);
	if (binary) {
		safe_fread(*vec, sizeof(real), (long) (*vecSize), f);
	} else {
		for (int i = 0; i < *vecSize; ++i) {
			if (sizeof(real) == 4) { //real is currently float
				if(fscanf(f, "%f", (float*)&((*vec)[i]))!=1) print_error("Problem reading from fscanf");
			} else {
				if(fscanf(f, "%lf", &((*vec)[i]))!=1) print_error("Problem reading from fscanf");
			}
		}
	}
}
#ifdef USE_BLAS
/**
 * Wrapper over cblas copy to make it type independent
 */
void cblas_xcopy(float* output,float* input,int size){
	cblas_scopy(size, input, 1, output, 1);
}
/**
 * Wrapper over cblas copy to make it type independent
 */
void cblas_xcopy(double* output,double* input,int size){
	cblas_dcopy(size, input, 1, output, 1);
}
/**
 * Wrapper over cblas matrix vector multiplication  to make it type independent
 */
void cblas_xgemv(float* output,int output_size,float* input,int input_size,float* biases,float* weights){
	cblas_sgemv(CblasColMajor, CblasTrans, input_size, output_size, 1.0, weights, input_size, input, 1, (biases ? 1.0 : 0.0), output, 1);
}
/**
 * Wrapper over cblas matrix vector multiplication  to make it type independent
 */
void cblas_xgemv(double* output,int output_size,double* input,int input_size,double* biases,double* weights){
	cblas_dgemv(CblasColMajor, CblasTrans, input_size, output_size, 1.0, weights, input_size, input, 1, (biases ? 1.0 : 0.0), output, 1);
}
#endif

