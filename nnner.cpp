#include <iostream>
#include "Hash.h"
#include "nnlib.h"
#include "Tokenizer.h"
#include <time.h>
#include <string>
#include <vector>
#include "NERConfiguration.h"
using namespace std;
static void usage();
static void parseCmdArgs(int arc,char** argv,const char*& cfgFile,const char*& scope,bool &train);
/*! \mainpage Named Entity Recognition System
 *
 * \section Introduction
 *
 * A neural network based named entity recognition system. This code includes a trainer module to
 * train the network. A testing module to test the tagging on individual files and also server module
 * for the interface to query the model on demand.
 *
 * \section  Installation
 * This module needs g++ to compile the code.
 * \subsection step1 Step 1: Installing external dependencies
 * Code uses CBLAS libraries to optimize the linear algebra functions.
 * For Ubuntu systems - sudo apt-get install libblas-* should install all the needed libraries to your system
 * \subsection step2 Step 2: Building the code
 * To generate the binaries
 * 	- Go to the directory containing cpp files and execute either of the following commands
 * 	- make (To generate both trainer and server executable)
 * 	- make nnner [generates only trainer]
 * 	- make nnner_server [generates only the server executable]
 * \subsection step3 Step 3: Modify config file
 * A sample nnner.cfg has been included with the code. Provide all the details of each file needed to
 * execute the code. Configuration file is self documented.
 * \subsection step4 Step 4 : Execute the code
 * Now you should be able to execute the code using following commands
 *  - ./nnner -cfg \<config_file\> -scope \<scope\> [train/test]
 *  - ./nnner_server \<config_file\> -scope \<scope\> -port \<port_number\>
 *
 */

/**
 * Main to enter the code for training or testing the NER system.
 * This function reads the config file provided in command line and
 * and trains or test the model. All params are to be defined even if
 * they are null.
 */
 int main(int argc,char** argv){
	NERConfiguration* config = new NERConfiguration();
	const char * cfgsource ;
	const char * scope;
	bool train=true;
	real lr,alpha;
	int epochs;
	int exitStatus=0;
	int hidden_layer_size=300,windowSize = 5;
	setlocale(LC_ALL,"");
	parseCmdArgs(argc,argv,cfgsource,scope,train);
	if(strcmp(cfgsource,"")==0 || strcmp(scope,"")==0) usage();
	try{

		config->parse(cfgsource,scope);
		hidden_layer_size = config->lookupInt("hiddenLayer");
		windowSize = config->lookupInt("windowSize");
		lr = config->lookupReal("learningRate");
		alpha = config->lookupReal("momentum");
		epochs = config->lookupInt("epochs");
		string rootPath = config->lookupString("rootDir");
		string outPath = config->lookupString("outputDir");
		string trainFilename =rootPath+string(config->lookupString("trainFile"));
		string testFilename = rootPath+string(config->lookupString("testFile"));
		string validFilename = rootPath + string(config->lookupString("validFile"));
		string paramFile = rootPath + string(config->lookupString("paramFile"));
		string wordHashFile = rootPath+ string(config->lookupString("wordHashFile"));
		string embeddingFile = string(rootPath)+string(config->lookupString("embeddingFile"));
		string nerHashFile = rootPath + string(config->lookupString("nerHashFile"));

		bool useCap = config->lookupBoolean("useCap");
		string capFeatureFile = rootPath+string(config->lookupString("capFeatureFile"));

		bool useGaz = config->lookupBoolean("useGaz");
		string gazFile = rootPath+string(config->lookupString("gazFile"));
		string gazpFile = rootPath+string(config->lookupString("gazpFile"));
		string gazoFile = rootPath+string(config->lookupString("gazoFile"));
		string gazlFile = rootPath+string(config->lookupString("gazlFile"));
		string gazmFile = rootPath+string(config->lookupString("gazmFile"));
		bool useBDLL = config->lookupBoolean("useBDLL");
		string BDLLFile = rootPath + config->lookupString("BDLLFile");
		bool useDICE = config->lookupBoolean("useDICE");
		string DICEFile = rootPath + config->lookupString("DICEFile");
		bool useEntropy = config->lookupBoolean("useEntropy");
		string EntropyFile = rootPath + config->lookupString("EntropyFile");
		bool useNPMI = config->lookupBoolean("useNPMI");
		string  NPMIFile =rootPath + config->lookupString("NPMIFile");
		bool useResIDF = config->lookupBoolean("useResIDF");
		string  resIDFFile =rootPath + config->lookupString("resIDFFile");


		Hash word_hash (wordHashFile.c_str());
		Hash ner_hash (nerHashFile.c_str());
		Hash* cap_hash=NULL;
		Feature* capFeature=NULL;
		Hash* gaz_hash=NULL;
		Hash* gazm_hash=NULL;
		Feature* gazmFeature=NULL;
		Hash* gazl_hash=NULL;
		Feature* gazlFeature=NULL;
		Hash* gazo_hash=NULL;
		Feature* gazoFeature=NULL;
		Hash* gazp_hash=NULL;
		Feature* gazpFeature=NULL;
		Hash* bdll_hash=NULL;
		Feature* bdllFeature=NULL;
		Hash* dice_hash=NULL;
		Feature* diceFeature=NULL;
		Hash* entropy_hash=NULL;
		Feature* entropyFeature=NULL;
		Hash* npmi_hash=NULL;
		Feature* npmiFeature=NULL;
		Hash* residf_hash=NULL;
		Feature* residfFeature=NULL;

		vector<Tokens> tokens;
		vector<Tokens> testTokens;
		vector<Tokens> validTokens;
		Tokenizer tokenizer(&word_hash,&ner_hash);
		int padidx= word_hash.getIndex("PADDING");
		NNNer ner(embeddingFile.c_str(),windowSize,hidden_layer_size,ner_hash.size(),padidx,lr,alpha,epochs,false,outPath.c_str());

		if(useCap){
			cap_hash = new Hash(capFeatureFile.c_str());
			capFeature= new Feature(cap_hash,5);
			tokenizer.setCapHash(cap_hash);
			ner.setCapFeature(capFeature);
		}
		/* Gaz tokens*/
		gaz_hash = new Hash(gazFile.c_str());
		tokenizer.setGaztHash(gaz_hash);

		if(useGaz){

			/* Gazetteer person*/
			gazp_hash =new Hash(gazpFile.c_str());
			gazpFeature = new Feature(gaz_hash,2);
			tokenizer.setGazpHash(gazp_hash);
			ner.setGazpFeature(gazpFeature);

			/* Gazetteer organization*/
			gazo_hash =new Hash(gazoFile.c_str());
			gazoFeature = new Feature(gaz_hash,2);
			tokenizer.setGazoHash(gazo_hash);
			ner.setGazoFeature(gazoFeature);

			/* Gazetteer location*/
			gazl_hash  =new Hash(gazlFile.c_str());
			gazlFeature = new Feature(gaz_hash,2);
			tokenizer.setGazlHash(gazl_hash);
			ner.setGazlFeature(gazlFeature);

			/* Gazetteer misc*/
			gazm_hash =new Hash(gazmFile.c_str());
			gazmFeature = new Feature(gaz_hash,2);
			tokenizer.setGazmHash(gazm_hash);
			ner.setGazmFeature(gazmFeature);
		}
		if(useBDLL){
			bdll_hash =new Hash(BDLLFile.c_str());
			bdllFeature = new Feature(gaz_hash,2);
			tokenizer.setBdllHash(bdll_hash);
			ner.setBdllFeature(bdllFeature);
		}
		if(useDICE){
			dice_hash =new Hash(DICEFile.c_str());
			diceFeature = new Feature(gaz_hash,2);
			tokenizer.setDiceHash(dice_hash);
			ner.setDiceFeature(diceFeature);
		}
		if(useEntropy){
			entropy_hash =new Hash(EntropyFile.c_str());
			entropyFeature = new Feature(gaz_hash,2);
			tokenizer.setEntropyHash(entropy_hash);
			ner.setEntropyFeature(entropyFeature);
		}
		if(useNPMI){
			npmi_hash =new Hash(NPMIFile.c_str());
			npmiFeature = new Feature(gaz_hash,2);
			tokenizer.setNpmiHash(npmi_hash);
			ner.setNpmiFeature(npmiFeature);
		}
		if(useResIDF){
			residf_hash =new Hash(resIDFFile.c_str());
			residfFeature = new Feature(gaz_hash,2);
			tokenizer.setResIdfHash(residf_hash);
			ner.setResIdfFeature(residfFeature);
		}
		if(train){
			tokenizer.tokenize_column_format(trainFilename.c_str(),tokens);
			tokenizer.tokenize_column_format(testFilename.c_str(),testTokens);
			ner.setParamFile(paramFile.c_str());
			ner.learnNet(tokens,testTokens);
		}else{
			string sent="";
			/* load paramFile*/
			ner.setParamFile(paramFile.c_str());
			ner.restoreNNet();
			while(sent != "end"){
				getline(cin,sent);
				Tokens tok = tokenizer.tokenize_sent_format(sent);
				ner.test(tok);
				tok.pretty_print();
			}
		}
//		ner.test(testTokens);
//		FILE* f = safe_fopen("tagged_output.txt","w");
//		for(int i=0;i<testTokens.size();i++){
//			testTokens[i].writeData(f);
//			fprintf(f,"\n");
//		}
//		safe_fclose(f);
		delete cap_hash;
		delete capFeature;
		delete gaz_hash;
		delete gazm_hash;
		delete gazmFeature;
		delete gazl_hash;
		delete	gazlFeature;
		delete	gazo_hash;
		delete	gazoFeature;
		delete	gazp_hash;
		delete	gazpFeature;
		delete bdll_hash;
		delete	bdllFeature;
		delete dice_hash;
		delete	diceFeature;
		delete	entropy_hash;
		delete	entropyFeature;
		delete	npmi_hash;
		delete	npmiFeature;
		delete residf_hash;
		delete residfFeature;

	}catch(const NERConfigurationException & e){
		fprintf(stderr,"%s\n",e.c_str());
		exitStatus=1;
	}
	delete config;
	return exitStatus;
}

 /**
  * \brief Command line argument parser<br>
  * Format - \<switch\> \<value\>
  *
  * @param argc - number of arguments
  * @param argv - command line arguments
  * @param cfgSource - configuration file
  * @param scope - configuration scope to be used
  * @param train - return param to decide whether to run training algo or just the decoder
  */
static void
parseCmdArgs(int	argc,
		char **		argv,
		const char *&		cfgSource,
		const char *&		scope,
		bool& train){

	int	i;
	cfgSource = "";
	scope = "";

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			usage();
		} else if (strcmp(argv[i], "-cfg") == 0) {
			if (i == argc-1) { usage(); }
			cfgSource = argv[i+1];
			i++;
		} else if (strcmp(argv[i], "-scope") == 0) {
			if (i == argc-1) { usage(); }
			scope = argv[i+1];
			i++;
		} else if (strcmp(argv[i], "-train") == 0) {
			train=true;
			i++;
		}else if(strcmp(argv[i], "-test") == 0) {
			train=false;
			i++;
		}
		else {
			fprintf(stderr, "Unrecognised option '%s'\n\n", argv[i]);
			usage();
		}
	}
}
/**
 * Prints the usage detail to run the code
 *
 */
static void usage(){
	fprintf(stderr,"\n"
			"Usage:nnner <options>\n"
			"<options> are\n"
			"\t-h\t\t\tPrint help\n"
			"\t-cfg <source>\t\tUse the specified configuration file(nnner.config)\n"
			"\t-scope <name>\t\tCorpus configuration scope\n"
			"\t[train/test]\t\t Use system for training or testing purpose"
			"\n");
	exit(1);
}
