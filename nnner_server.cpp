#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>

#include <iostream>
#include "Hash.h"
#include "nnlib.h"
#include "Tokenizer.h"
#include <time.h>
#include <string>
#include <vector>
#include "utils.h"
#include "NERConfiguration.h"
using namespace std;

static void usage();
static void parseCmdArgs(int arc, char** argv, const char*& cfgFile,
		const char*& scope, bool &train);
/* fgets max sizes */
#define MAX_SENTENCE_SIZE 16024

char PORT[10] = "11112";  // the port users will be connecting to

#define BACKLOG 10 // how many pending connections queue will hold
/*globals*/
char buf[MAX_SENTENCE_SIZE];
char resp[2 * MAX_SENTENCE_SIZE] = { 0 };
Hash* word_hash;
Hash* ner_hash;
Hash* cap_hash = NULL;
Feature* capFeature = NULL;
Hash* gaz_hash = NULL;
Hash* gazm_hash = NULL;
Feature* gazmFeature = NULL;
Hash* gazl_hash = NULL;
Feature* gazlFeature = NULL;
Hash* gazo_hash = NULL;
Feature* gazoFeature = NULL;
Hash* gazp_hash = NULL;
Feature* gazpFeature = NULL;
Hash* bdll_hash = NULL;
Feature* bdllFeature = NULL;
Hash* dice_hash = NULL;
Feature* diceFeature = NULL;
Hash* entropy_hash = NULL;
Feature* entropyFeature = NULL;
Hash* npmi_hash = NULL;
Feature* npmiFeature = NULL;
Hash* residf_hash = NULL;
Feature* residfFeature = NULL;
Tokenizer* tokenizer = NULL;
NNNer* ner = NULL;
/**
 * Used for handling the child process
 */
void sigchld_handler(int s) {
	while (waitpid(-1, NULL, WNOHANG) > 0)
		;
}

/**
 *  get sockaddr, IPv4 or IPv6: for the current system
 */
void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*) sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*) sa)->sin6_addr);
}
/**
 * Free up all the allocated memory for the NER model
 */
void freeMemory() {
	delete cap_hash;
	delete capFeature;
	delete gaz_hash;
	delete gazm_hash;
	delete gazmFeature;
	delete gazl_hash;
	delete gazlFeature;
	delete gazo_hash;
	delete gazoFeature;
	delete gazp_hash;
	delete gazpFeature;
	delete bdll_hash;
	delete bdllFeature;
	delete dice_hash;
	delete diceFeature;
	delete entropy_hash;
	delete entropyFeature;
	delete npmi_hash;
	delete npmiFeature;
	delete residf_hash;
	delete residfFeature;
	delete word_hash;
	delete ner_hash;
	delete ner;
	delete tokenizer;
}
/**
 * NER module initializer. Reads the configuration file supplied
 * in command line argument and sets all the required parameters
 * @param  argc - number of command line arguments
 * @param  argv - command line arguments
 */
int initNER(int argc, char** argv) {
	NERConfiguration* config = new NERConfiguration();
	const char * cfgsource;
	const char * scope;
	bool train = true;
	real lr, alpha;
	int epochs;
	int exitStatus = 0;
	int hidden_layer_size = 300, windowSize = 5;
	setlocale(LC_ALL, "");
	parseCmdArgs(argc, argv, cfgsource, scope, train);
	if (strcmp(cfgsource, "") == 0 || strcmp(scope, "") == 0)
		usage();
	try {

		config->parse(cfgsource, scope);
		hidden_layer_size = config->lookupInt("hiddenLayer");
		windowSize = config->lookupInt("windowSize");
		lr = config->lookupReal("learningRate");
		alpha = config->lookupReal("momentum");
		epochs = config->lookupInt("epochs");
		string rootPath = config->lookupString("rootDir");
		string outPath = config->lookupString("outputDir");
		string trainFilename = rootPath
				+ string(config->lookupString("trainFile"));
		string testFilename = rootPath
				+ string(config->lookupString("testFile"));
		string validFilename = rootPath
				+ string(config->lookupString("validFile"));
		string paramFile = rootPath + string(config->lookupString("paramFile"));
		string wordHashFile = rootPath
				+ string(config->lookupString("wordHashFile"));
		string embeddingFile = string(rootPath)
				+ string(config->lookupString("embeddingFile"));
		string nerHashFile = rootPath
				+ string(config->lookupString("nerHashFile"));

		bool useCap = config->lookupBoolean("useCap");
		string capFeatureFile = rootPath
				+ string(config->lookupString("capFeatureFile"));

		bool useGaz = config->lookupBoolean("useGaz");
		string gazFile = rootPath + string(config->lookupString("gazFile"));
		string gazpFile = rootPath + string(config->lookupString("gazpFile"));
		string gazoFile = rootPath + string(config->lookupString("gazoFile"));
		string gazlFile = rootPath + string(config->lookupString("gazlFile"));
		string gazmFile = rootPath + string(config->lookupString("gazmFile"));
		bool useBDLL = config->lookupBoolean("useBDLL");
		string BDLLFile = rootPath + config->lookupString("BDLLFile");
		bool useDICE = config->lookupBoolean("useDICE");
		string DICEFile = rootPath + config->lookupString("DICEFile");
		bool useEntropy = config->lookupBoolean("useEntropy");
		string EntropyFile = rootPath + config->lookupString("EntropyFile");
		bool useNPMI = config->lookupBoolean("useNPMI");
		string NPMIFile = rootPath + config->lookupString("NPMIFile");
		bool useResIDF = config->lookupBoolean("useResIDF");
		string resIDFFile = rootPath + config->lookupString("resIDFFile");

		word_hash = new Hash(wordHashFile.c_str());
		ner_hash = new Hash(nerHashFile.c_str());

		tokenizer = new Tokenizer(word_hash, ner_hash);
		int padidx = word_hash->getIndex("PADDING");
		ner = new NNNer(embeddingFile.c_str(), windowSize, hidden_layer_size,
				ner_hash->size(), padidx, lr, alpha, epochs, false,
				outPath.c_str());

		if (useCap) {
			cap_hash = new Hash(capFeatureFile.c_str());
			capFeature = new Feature(cap_hash, 5);
			tokenizer->setCapHash(cap_hash);
			ner->setCapFeature(capFeature);
		}
		/* Gaz tokens*/
		gaz_hash = new Hash(gazFile.c_str());
		tokenizer->setGaztHash(gaz_hash);

		if (useGaz) {

			/* Gazetteer person*/
			gazp_hash = new Hash(gazpFile.c_str());
			gazpFeature = new Feature(gaz_hash, 2);
			tokenizer->setGazpHash(gazp_hash);
			ner->setGazpFeature(gazpFeature);

			/* Gazetteer organization*/
			gazo_hash = new Hash(gazoFile.c_str());
			gazoFeature = new Feature(gaz_hash, 2);
			tokenizer->setGazoHash(gazo_hash);
			ner->setGazoFeature(gazoFeature);

			/* Gazetteer location*/
			gazl_hash = new Hash(gazlFile.c_str());
			gazlFeature = new Feature(gaz_hash, 2);
			tokenizer->setGazlHash(gazl_hash);
			ner->setGazlFeature(gazlFeature);

			/* Gazetteer misc*/
			gazm_hash = new Hash(gazmFile.c_str());
			gazmFeature = new Feature(gaz_hash, 2);
			tokenizer->setGazmHash(gazm_hash);
			ner->setGazmFeature(gazmFeature);
		}
		if (useBDLL) {
			bdll_hash = new Hash(BDLLFile.c_str());
			bdllFeature = new Feature(gaz_hash, 2);
			tokenizer->setBdllHash(bdll_hash);
			ner->setBdllFeature(bdllFeature);
		}
		if (useDICE) {
			dice_hash = new Hash(DICEFile.c_str());
			diceFeature = new Feature(gaz_hash, 2);
			tokenizer->setDiceHash(dice_hash);
			ner->setDiceFeature(diceFeature);
		}
		if (useEntropy) {
			entropy_hash = new Hash(EntropyFile.c_str());
			entropyFeature = new Feature(gaz_hash, 2);
			tokenizer->setEntropyHash(entropy_hash);
			ner->setEntropyFeature(entropyFeature);
		}
		if (useNPMI) {
			npmi_hash = new Hash(NPMIFile.c_str());
			npmiFeature = new Feature(gaz_hash, 2);
			tokenizer->setNpmiHash(npmi_hash);
			ner->setNpmiFeature(npmiFeature);
		}
		if (useResIDF) {
			residf_hash = new Hash(resIDFFile.c_str());
			residfFeature = new Feature(gaz_hash, 2);
			tokenizer->setResIdfHash(residf_hash);
			ner->setResIdfFeature(residfFeature);
		}
		ner->setParamFile(paramFile.c_str());
		ner->restoreNNet();
		//		ner.test(testTokens);
		//		FILE* f = safe_fopen("tagged_output.txt","w");
		//		for(int i=0;i<testTokens.size();i++){
		//			testTokens[i].writeData(f);
		//			fprintf(f,"\n");
		//		}
		//		safe_fclose(f);

	} catch (const NERConfigurationException & e) {
		fprintf(stderr, "%s\n", e.c_str());
		exitStatus = 1;
	}
	delete config;
	return exitStatus;

}
/**
 * Driver function to run a query with the NER module
 * @param query - Sentence input to the NER system
 * @param resp - Response to the input as NE tagged words
 */
void testNER(char* query, char* resp) {

	/* load paramFile*/
	string sent(query);
	string res = "";
	vector<string> sents = split(sent,'.');
	for(int i=0;i<sents.size();i++){
		string r;
		trim(sents[i]);
		if(sents[i].length()==0) continue;
		Tokens tok = tokenizer->tokenize_sent_format(sents[i]);
		if (!ner)
			cout << "NULL" << endl;
		ner->test(tok);
		tok.pretty_write(r);
		trim(r);
		res+=r+" .\n";
	}

	strcpy(resp, res.c_str());
	cout << "done" << endl;

}
/**
 * Control Logic of the code. Creates a server listening on PORT for incoming
 * queries
 * @param  argc - number of command line arguments
 * @param  argv - command line arguments
 *
 */
int main(int argc, char** argv) {

//	initNER(argc,argv);
//	char* sap = "India is a big country";
//	testNER(sap,resp);
//	cout<<resp<<endl;
	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size;
	struct sigaction sa;
	int yes = 1;
	char s[INET6_ADDRSTRLEN];
	int rv;
	FILE * logFile;
	char parent[] = "parent persist";

	initNER(argc, argv);
	logFile = fopen("log.txt", "a+");
	if (!logFile)
		print_error("Could not open log file");
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol))
				== -1) {
			perror("server: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int))
				== -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			cout << PORT << endl;
			perror("server: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}

	freeaddrinfo(servinfo); // all done with this structure
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}

	sa.sa_handler = sigchld_handler; // reap all dead processes
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;
	if (sigaction(SIGCHLD, &sa, NULL) == -1) {
		perror("sigaction");
		exit(1);
	}

	printf("server: waiting for connections...\n");
	while (1) {  // main accept() loop

		sin_size = sizeof their_addr;
		new_fd = accept(sockfd, (struct sockaddr *) &their_addr, &sin_size);
		if (new_fd == -1) {
			perror("accept");
			continue;
		}

		inet_ntop(their_addr.ss_family,
				get_in_addr((struct sockaddr *) &their_addr), s, sizeof s);
		printf("server: got connection from %s\n", s);
//		if (!fork()) { // this is the child process

		memset(buf, 0, sizeof(char) * MAX_SENTENCE_SIZE);
		memset(resp, 0, 2 * sizeof(char) * MAX_SENTENCE_SIZE);
//			close(sockfd); // child doesn't need the listener
		if (recv(new_fd, buf, 1000, 0) < 0) {
			perror("recv");
		}
		printf("QUERY: %s\n", buf);
		fprintf(logFile, "QUERY: %s\n", buf);
		testNER(buf, resp);
		cout << resp << endl;
		fprintf(logFile, "RESPO: %s\n", resp);
		cout << resp << endl;
		if (send(new_fd, resp, 2 * MAX_SENTENCE_SIZE, 0) == -1)
			perror("send");
//		close(new_fd);
		fflush(logFile);
//		exit(0);
//		}
		close(new_fd);  // parent doesn't need this

	}
	fclose(logFile);
	freeMemory();
}

/**
 * \brief Command line argument parser<br>
 * Format - \<switch\> \<value\>
 *
 * @param argc - number of arguments
 * @param argv - command line arguments
 * @param cfgSource - configuration file
 * @param scope - configuration scope to be used
 * @param train - return param to decide whether to run training algo or just the decoder
 */
static void parseCmdArgs(int argc, char ** argv, const char *& cfgSource,
		const char *& scope, bool& train) {

	int i;
	cfgSource = "";
	scope = "";

	for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-h") == 0) {
			usage();
		} else if (strcmp(argv[i], "-cfg") == 0) {
			if (i == argc - 1) {
				usage();
			}
			cfgSource = argv[i + 1];
			i++;
		} else if (strcmp(argv[i], "-scope") == 0) {
			if (i == argc - 1) {
				usage();
			}
			scope = argv[i + 1];
			i++;
		} else if (strcmp(argv[i], "-port") == 0) {
			if (i == argc - 1) {
				usage();
			}
			strcpy(PORT, argv[i + 1]);
			i++;
		} else {
			fprintf(stderr, "Unrecognised option '%s'\n\n", argv[i]);
			usage();
		}
	}
}
/**
 * Prints the usage detail to run the code
 *
 */
static void usage() {
	fprintf(stderr,
			"\n"
					"Usage:nnner <options>\n"
					"<options> are\n"
					"\t-h\t\t\tPrint help\n"
					"\t-cfg <source>\t\tUse the specified configuration file(nnner.config)\n"
					"\t-scope <name>\t\tCorpus configuration scope\n"
					"\t-port <int>\t\tport to be used by server\n"
					"\n");
	exit(1);
}
