IDIR=include
ILIB=lib
CC=g++
OPT = -O3  -ffast-math
CFLAGS = -I$(IDIR) -L$(ILIB) 
CDEF = -DUSE_BLAS
DEPS = nnlib.h Tokenizer.h Hash.h utils.h NERConfiguration.h
LIBS = -lblas -lconfig4cpp 
ODIR=obj

_ROBJ = nnlib.o Tokenizer.o Hash.o utils.o NERConfiguration.o
ROBJ = $(patsubst %,$(ODIR)/%,$(_ROBJ))

_NOBJ = nnner.o
NOBJ = $(patsubst %,$(ODIR)/%,$(_NOBJ))

_SOBJ = nnner_server.o
SOBJ = $(patsubst %,$(ODIR)/%,$(_SOBJ))

dir_exists=@mkdir -p $(@D)

$(ODIR)/%.o : %.cpp $(DEPS)
	$(dir_exists)
	$(CC) -c $(CDEF) $(OPT) -o $@ $< $(CFLAGS)
all:nnner nnner_server
nnner: $(NOBJ) $(ROBJ)
	$(CC) -o $@ $(OPT) $^ $(CFLAGS) $(LIBS)
	@echo 'Finished building $@'
	@echo ' '
nnner_server: $(SOBJ) $(ROBJ)
	$(CC) -o $@ $(OPT) $^ $(CFLAGS) $(LIBS)
	@echo 'Finished building $@'
	@echo ' '

.PHONY: clean all

clean:
	rm -rf $(OBJ)/*.o core *~ nnner nnner_server $(ODIR) *.out