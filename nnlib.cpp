#include "nnlib.h"

#define ISPROB(x) ((x)>=0 && (x)<=1)
NNNer::NNNer(const char* wordVecFile, int windowSize, int hiddenStateSize,
		int outputVecSize, int padidx, real lr, real alpha,int max_epoch, bool binary,const char* oPath) :
		window_size(windowSize), hidden_state_size(hiddenStateSize), binary(
				binary), starting_lr(lr), alpha(alpha),max_epoch(max_epoch) {
	FILE* f;
	if(oPath) strcpy(outputPath,oPath);
	f = safe_fopen(wordVecFile, "rb");
	read_vector_2d(&word_lookup_table, &vocabSize, &wordVecSize, f, binary);
	safe_fclose(f);
	input_state = NULL;
	input_state_size = wordVecSize;
	output_state_size = outputVecSize;
	hidden_state = (real*) allocMemory(sizeof(real), hidden_state_size);
	output_state = NULL;
	labels = NULL;
	l1_weight = NULL;
	l2_weight = NULL;
	l2_bias = NULL;
	l1_bias = NULL;
	viterbi_score_init = NULL;
	viterbi_score_trans = NULL;
	word_padding_idx = padidx;
	capFeature = NULL;
	gazlFeature = NULL;
	gazmFeature = NULL;
	gazoFeature = NULL;
	gazpFeature = NULL;
	bdllFeature =NULL;
	resIdfFeature = NULL;
	entropyFeature =NULL;
	npmiFeature =NULL;
	diceFeature =NULL;
	prev_l1_weight = NULL;
	prev_l2_weight = NULL;
	prev_l2_bias = NULL;
	prev_l1_bias = NULL;
	this->lr=starting_lr;
	prev_cost=0;
}
/**
 * Destructor
 */
NNNer::~NNNer() {
	free(input_state);
	free(output_state);
	free(word_lookup_table);
	free(hidden_state);
	free(l1_bias);
	free(l2_bias);
	free(l2_weight);
	free(l1_weight);
	free(prev_l1_weight) ;
	free(prev_l2_weight);
	free(prev_l2_bias);
	free(prev_l1_bias);
	free(viterbi_score_init);
	free(viterbi_score_trans);
}

void NNNer::setTrainFile(const char* filename) {
	strcpy(trainFile, filename);
}
void NNNer::setValidFile(const char* filename) {
	strcpy(validFile, filename);
}
void NNNer::setTestFile(const char* filename) {
	strcpy(testFile, filename);
}
void NNNer::setParamFile(const char* filename) {
	strcpy(paramFile,filename);
}

/**
 * Generates a random number from a uniform distribution between the range min and max
 * @param min - lower bound
 * @param max - upper bound
 * @return Random number between min and max
 */
real NNNer::random(real min, real max) {
	return rand() / (real) RAND_MAX * (max - min) + min;
}
/**
 * Hardtanh transformation layer. This layer bounds the output of the neuron in the range -1 and 1
 * @param output - output vector
 * @param input - input vector
 * @param size - size of input/output vector
 * @return A real vector in output
 */
void NNNer::hardtanh_layer(real *output, real *input, int size) {
	int i;
	float z = 0;
	for (i = 0; i < size; i++) {
		z = input[i];
		if (!z)
			printf("Problem\n");
		if (z >= -1 && z <= 1)
			output[i] = z;
		else if (z < -1)
			output[i] = -1;
		else
			output[i] = 1;
		z = 0;
	}
}
/**
 * Performs a linear projection of the input vector. F = WX + B
 * @param  output - output vector pointer
 * @param  output_size - output vector size
 * @param weights - Weight matrix W
 * @param biases - bias parameters  B
 * @param input - input vector X
 * @param input_size - input vector size
 * @return A real vector in output
 */
void NNNer::linear_layer(real *output, int output_size, real *weights,
		real *biases, real *input, int input_size) {
#ifdef USE_BLAS
	if (biases)
		cblas_xcopy(output, biases, output_size);
	cblas_xgemv(output, output_size, input, input_size, biases, weights);
#else
	int i, j;
	for (i = 0; i < output_size; i++) {
		real z = (biases ? biases[i] : 0);
		real *weights_row = weights + i * input_size;
		for (j = 0; j < input_size; j++)
		z += input[j] * weights_row[j];
		output[i] = z;
	}
#endif
}
/**
 * Sigmoid layer transformation. Applies sigmoidal transformation to each
 * point in vector
 * @param output - output vector pointer
 * @param input - input vector pointer
 * @param input_size - input/output vector size
 * @return A real vector in \p output
 */
void NNNer::sigmoid_layer(real *output,real *input, int input_size) {
	for(int i=0;i<input_size;i++){
		output[i] = 1.0/(1+exp(-input[i]));
	}
}
/**
 * Neural network initializer function . Allocates and initializes the weight parameter with
 * random numbers.
 */
void NNNer::initNet() {
	int a, b;

	l1_weight = (real*) allocMemory(sizeof(real),
			window_size * input_state_size * hidden_state_size);
	l1_bias = (real*) allocMemory(sizeof(real), hidden_state_size);
	l2_weight = (real*) allocMemory(sizeof(real),
			output_state_size * hidden_state_size);
	l2_bias = (real*) allocMemory(sizeof(real), output_state_size);

	prev_l1_weight = (real*) allocMemory(sizeof(real),
			window_size * input_state_size * hidden_state_size);
	prev_l1_bias = (real*) allocMemory(sizeof(real), hidden_state_size);
	prev_l2_weight = (real*) allocMemory(sizeof(real),
			output_state_size * hidden_state_size);
	prev_l2_bias = (real*) allocMemory(sizeof(real), output_state_size);

	for (b = 0; b < hidden_state_size; ++b) {
		for (a = 0; a < input_state_size * window_size; ++a) {
			l1_weight[IDXR(b,a,input_state_size*window_size)] = random(-0.001,
					0.001) + random(-0.001, 0.001) + random(-0.001, 0.001);
		}
		l1_bias[b] = random(-0.001, 0.001) + random(-0.001, 0.001)
				+ random(-0.001, 0.001);
	}
	for (b = 0; b < output_state_size; ++b) {
		for (a = 0; a < hidden_state_size; ++a) {
			l2_weight[IDXR(b,a,hidden_state_size)] = random(-0.1, 0.1)
					+ random(-0.001, 0.001) + random(-0.001, 0.001);
		}
		l2_bias[b] = random(-0.001, 0.001) + random(-0.001, 0.001)
				+ random(-0.001, 0.001);
	}
	viterbi_score_init = (real*) allocMemory(sizeof(real), output_state_size);
	viterbi_score_trans = (real*) allocMemory(sizeof(real),
			output_state_size * output_state_size);
	for (b = 0; b < output_state_size; ++b) {
		for (a = 0; a < output_state_size; ++a) {
			viterbi_score_trans[IDXR(b,a,output_state_size)] = random(-0.001,
					0.001) + random(-0.001, 0.001) + random(-0.001, 0.001);
		}
		viterbi_score_init[b] = random(-0.001, 0.001) + random(-0.001, 0.001)
				+ random(-0.001, 0.001);
	}
	memset(prev_l1_weight, 0,
			sizeof(real) * input_state_size * window_size * hidden_state_size);
	memset(prev_l1_bias, 0, sizeof(real) * hidden_state_size);
	memset(prev_l2_weight, 0,
			sizeof(real) * output_state_size * hidden_state_size);
	memset(prev_l2_bias, 0, sizeof(real) * output_state_size);

}
/**
 * A lookup table function to convert a word ids to their corresponding
 * word vectors.
 * @param dest - output vector pointer
 * @param stride - skip size
 * @param weights - lookup table
 * @param wordVecSize - size of each vector
 * @param maxidx - max index in lookup table
 * @param wordIndices - word idx input vector
 * @param numWords - number of word indices
 * @param padidx - index of pad to use
 * @param npad - number of pads to apply before and after the vector
 * @return A vector of concatenation of word lookups in \p dest
 */
void NNNer::nnLookup(real* dest, int stride, const real* weights,
		int wordVecSize, int maxidx, const int* wordIndices, int numWords,
		int padidx, int npad) {
	int i;
	if (padidx < 0 || padidx >= maxidx)
		print_error("lookup: padding index out of range");
	for (i = 0; i < npad; ++i) {
		memcpy(dest + i * stride, weights + padidx * wordVecSize,
				wordVecSize * sizeof(real));
	}
	for (i = 0; i < numWords; ++i) {
		int wordidx = wordIndices[i];
		if (wordidx < 0 || wordidx >= maxidx)
			print_error("lookup: wordIdx out of range");
		memcpy(dest + (i + npad) * stride, weights + wordidx * wordVecSize,
				wordVecSize * sizeof(real));
	}
	for (i = 0; i < npad; ++i) {
		memcpy(dest + (i + npad + numWords) * stride,
				weights + padidx * wordVecSize, wordVecSize * sizeof(real));
	}
}
/**
 * Stores the parameters of the network to the file
 * \todo store in non binary format
 */
void NNNer::storeNNet() {

	FILE* f = safe_fopen(paramFile, "wb");
	char validity_check[] = "abc";
	bool b =true;
	if (b) {
		fwrite(&window_size, sizeof(int), 1, f);
		fwrite(&wordVecSize, sizeof(int), 1, f); // Word vector size
		fwrite(&vocabSize, sizeof(int), 1, f); // Max vocabulary size
		fwrite(&input_state_size, sizeof(int), 1, f); // input vector size to network [window_size * wordVecSize]
		fwrite(&hidden_state_size, sizeof(int), 1, f); // hidden state size
		fwrite(&output_state_size, sizeof(int), 1, f); // output state size (number of tags )
		fwrite(word_lookup_table, sizeof(real), vocabSize * wordVecSize, f);
		// word vector table
		fwrite(l1_weight, sizeof(real),
				input_state_size * window_size * hidden_state_size, f); // layer 1 weights
		fwrite(l1_bias, sizeof(real), hidden_state_size, f); // layer 1 biases
		fwrite(l2_weight, sizeof(real), hidden_state_size * output_state_size,
				f); // layer 2 weight
		fwrite(l2_bias, sizeof(real), output_state_size, f); // layer 2 biases
		fwrite(viterbi_score_init, sizeof(real), output_state_size, f); // viterbi initial scores
		fwrite(viterbi_score_trans, sizeof(real),
				output_state_size * output_state_size, f); // viterbi transition scores
		fwrite(&word_padding_idx, sizeof(int), 1, f); //word padding index
		int c =capFeature==NULL;
		if(c){
			fwrite(&c,sizeof(int),1,f);
		}else{
			fwrite(&c,sizeof(int),1,f);
			capFeature->serialize(f);
		}
		c =gazpFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			gazpFeature->serialize(f);
		}
		c =gazlFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			gazlFeature->serialize(f);
		}
		c =gazoFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			gazoFeature->serialize(f);
		}
		c =gazmFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			gazmFeature->serialize(f);
		}
		c =bdllFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			bdllFeature->serialize(f);
		}
		c =diceFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			diceFeature->serialize(f);
		}
		c =npmiFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			npmiFeature->serialize(f);
		}
		c =entropyFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			entropyFeature->serialize(f);
		}
		c =resIdfFeature==NULL;
		fwrite(&c,sizeof(int),1,f);
		if(!c){
			resIdfFeature->serialize(f);
		}
		fwrite(validity_check, sizeof(char), sizeof(validity_check), f);
	} else {
		//TODO: implementation for future
	}
	safe_fclose(f);
}
/**
 * Restore Network param from the file \p paramFile
 * \todo restore from non binary format file
 */
void NNNer::restoreNNet() {
	FILE* f = safe_fopen(paramFile, "rb");
		char validity_check[] = "abc";
		bool b =true;
		size_t res;
		if (b) {
			res=fread(&window_size, sizeof(int), 1, f);
			if(res!=1) print_error("window size read problem");
			if(fread(&wordVecSize, sizeof(int), 1, f)!=1) print_error("wordvec size read problem"); // Word vector size
			if(fread(&vocabSize, sizeof(int), 1, f)!=1) print_error("vocab size read problem"); // Max vocabulary size
			if(fread(&input_state_size, sizeof(int), 1, f)!=1) print_error("input state size read problem"); // input vector size to network [window_size * wordVecSize]
			if(fread(&hidden_state_size, sizeof(int), 1, f)!=1) print_error("hidden state size read problem"); // hidden state size
			if(fread(&output_state_size, sizeof(int), 1, f)!=1) print_error("word lookup size read problem"); // output state size (number of tags )
			initNet();
			if(fread(word_lookup_table, sizeof(real), vocabSize * wordVecSize, f)!=vocabSize * wordVecSize) print_error("word look up table read problem");
			// word vector table
			if(fread(l1_weight, sizeof(real),
					input_state_size * window_size * hidden_state_size, f)!=input_state_size * window_size * hidden_state_size) print_error("l1 weight read problem"); // layer 1 weights
			if(fread(l1_bias, sizeof(real), hidden_state_size, f)!=hidden_state_size) print_error("l1 bias read problem"); // layer 1 biases
			if(fread(l2_weight, sizeof(real), hidden_state_size * output_state_size,
					f)!=hidden_state_size * output_state_size) print_error("l2 weight read problem"); // layer 2 weight
			if(fread(l2_bias, sizeof(real), output_state_size, f)!=output_state_size) print_error("l2 bias read problem"); // layer 2 biases
			if(fread(viterbi_score_init, sizeof(real), output_state_size, f)!=output_state_size) print_error("viterbi score init read problem"); // viterbi initial scores
			if(fread(viterbi_score_trans, sizeof(real),
					output_state_size * output_state_size, f)!=output_state_size * output_state_size) print_error("viterbi score trans read problem"); // viterbi transition scores
			if(fread(&word_padding_idx, sizeof(int), 1, f)!=1) print_error("word padding idx read problem"); //word padding index
			int c =capFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("cap check read problem");
			if(!c){
				capFeature->deserialize(f);
			}
			c =gazpFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("gazp check read problem");
			if(!c){
				gazpFeature->deserialize(f);
			}
			c =gazlFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("gazl check read problem");;
			if(!c){
				gazlFeature->deserialize(f);
			}
			c =gazoFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("gazo check read problem");;
			if(!c){
				gazoFeature->deserialize(f);
			}
			c =gazmFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("gazm check read problem");;
			if(!c){
				gazmFeature->deserialize(f);
			}
			c =bdllFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("bdll check read problem");;
			if(!c){
				bdllFeature->deserialize(f);
			}
			c =diceFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("dice check read problem");;
			if(!c){
				diceFeature->deserialize(f);
			}
			c =npmiFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("npmi check read problem");;
			if(!c){
				npmiFeature->deserialize(f);
			}
			c =entropyFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("entropy check read problem");;
			if(!c){
				entropyFeature->deserialize(f);
			}
			c =resIdfFeature==NULL;
			if(fread(&c,sizeof(int),1,f)!=1) print_error("resIDF check read problem");;
			if(!c){
				resIdfFeature->deserialize(f);
			}
			if(fread(validity_check, sizeof(char), sizeof(validity_check), f)==1) print_error("validity check string read problem");;
			if(strcmp(validity_check,"abc")!=0) print_error("Parameter file corrupt");
		} else {
			//TODO: implementation for future
		}
		safe_fclose(f);
}
/**
 * Neural network forward pass. Takes a sentence as input and apply the forward pass for each word
 * @param e - sentence in the form of tokens
 * @return A output vector for each word in the \p Tokens
 */
void NNNer::forwardPass(Tokens& e) {
	int psize;
	nnLookup(input_state, input_state_size, this->word_lookup_table,
			wordVecSize, vocabSize, e.word_idx, e.num_words, word_padding_idx,
			(window_size - 1) / 2);
	psize = wordVecSize;
	if (capFeature) {
		nnLookup(input_state + psize, input_state_size,
				capFeature->lookup_table, capFeature->vecSize,
				capFeature->featureHash->size(), e.cap_feature, e.num_words,
				capFeature->featureHash->getIndex("PADDING"),
				(window_size - 1) / 2);
		psize = psize + capFeature->vecSize;
	}
	if (gazpFeature) {
		nnLookup(input_state + psize, input_state_size,
				gazpFeature->lookup_table, gazpFeature->vecSize,
				gazpFeature->featureHash->size(), e.gazp_idx, e.num_words,
				gazpFeature->featureHash->getIndex("PADDING"),
				(window_size - 1) / 2);
		psize += gazpFeature->vecSize;
	}
	if (gazlFeature) {
		nnLookup(input_state + psize, input_state_size,
				gazlFeature->lookup_table, gazlFeature->vecSize,
				gazlFeature->featureHash->size(), e.gazl_idx, e.num_words,
				gazlFeature->featureHash->getIndex("PADDING"),
				(window_size - 1) / 2);
		psize += gazlFeature->vecSize;
	}
	if (gazoFeature) {
		nnLookup(input_state + psize, input_state_size,
				gazoFeature->lookup_table, gazoFeature->vecSize,
				gazoFeature->featureHash->size(), e.gazo_idx, e.num_words,
				gazoFeature->featureHash->getIndex("PADDING"),
				(window_size - 1) / 2);
		psize += gazoFeature->vecSize;
	}
	if (gazmFeature) {
		nnLookup(input_state + psize, input_state_size,
				gazmFeature->lookup_table, gazmFeature->vecSize,
				gazmFeature->featureHash->size(), e.gazm_idx, e.num_words,
				gazmFeature->featureHash->getIndex("PADDING"),
				(window_size - 1) / 2);
		psize += gazmFeature->vecSize;
	}
	if (bdllFeature) {
			nnLookup(input_state + psize, input_state_size,
					bdllFeature->lookup_table, bdllFeature->vecSize,
					bdllFeature->featureHash->size(), e.bdll_idx, e.num_words,
					bdllFeature->featureHash->getIndex("PADDING"),
					(window_size - 1) / 2);
			psize += bdllFeature->vecSize;
		}
	if (diceFeature) {
			nnLookup(input_state + psize, input_state_size,
					diceFeature->lookup_table, diceFeature->vecSize,
					diceFeature->featureHash->size(), e.dice_idx, e.num_words,
					diceFeature->featureHash->getIndex("PADDING"),
					(window_size - 1) / 2);
			psize += diceFeature->vecSize;
		}
	if (npmiFeature) {
			nnLookup(input_state + psize, input_state_size,
					npmiFeature->lookup_table, npmiFeature->vecSize,
					npmiFeature->featureHash->size(), e.npmi_idx, e.num_words,
					npmiFeature->featureHash->getIndex("PADDING"),
					(window_size - 1) / 2);
			psize += npmiFeature->vecSize;
		}
	if (entropyFeature) {
			nnLookup(input_state + psize, input_state_size,
					entropyFeature->lookup_table, entropyFeature->vecSize,
					entropyFeature->featureHash->size(), e.entropy_idx, e.num_words,
					entropyFeature->featureHash->getIndex("PADDING"),
					(window_size - 1) / 2);
			psize += entropyFeature->vecSize;
	}
	if (resIdfFeature) {
			nnLookup(input_state + psize, input_state_size,
					resIdfFeature->lookup_table, resIdfFeature->vecSize,
					resIdfFeature->featureHash->size(), e.residf_idx, e.num_words,
					resIdfFeature->featureHash->getIndex("PADDING"),
					(window_size - 1) / 2);
			psize += resIdfFeature->vecSize;
		}
	for (int idx = 0; idx < e.num_words; idx++) {
		linear_layer(hidden_state + idx * hidden_state_size, hidden_state_size,
				l1_weight, l1_bias, input_state + idx * input_state_size,
				window_size * input_state_size);
		sigmoid_layer(hidden_state + idx * hidden_state_size,
				hidden_state + idx * hidden_state_size, hidden_state_size);
		linear_layer(output_state + idx * output_state_size, output_state_size,
				l2_weight, l2_bias, hidden_state + idx * hidden_state_size,
				hidden_state_size);
		sigmoid_layer(output_state + idx * output_state_size,
				output_state + idx * output_state_size, output_state_size);
	}
//	print_vector_2d(l1_weight,hidden_state_size,input_state_size*window_size);
//	print_vector_1d(l1_bias,hidden_state_size);
//	print_vector_2d(l2_weight,output_state_size,hidden_state_size);
//	print_vector_1d(l2_bias,output_state_size);
//	print_vector_2d(input_state,e.num_words+window_size-1,input_state_size);
//	print_vector_2d(hidden_state,e.num_words,hidden_state_size);
//	print_vector_2d(output_state,e.num_words,output_state_size);

//labels = (int *)reallocMemory(labels, sizeof(int), sentence_size);
//viterbi(labels, viterbi_score_init, viterbi_score_trans, output_state, output_state_size, sentence_size);
}
/**
 * A non overflowing implementation for logarithm of sum of exponentials.
 * @param a - input vector
 * @param size - size of input vectors
 * @return log of sum of exponentiation of each term
 */
real logadd(real* a, int size) {
	if (size == 1)
		return a[0];
	real max_val = *max_element(a, a + size);
	real sum = 0.0;
	for (int i = 0; i < size; i++) {
		if (a[i] != -std::numeric_limits<real>::infinity()) {
			sum += exp(a[i] - max_val);
		}
	}
	return max_val + log(sum);
}
/**
 * Return sum of exponential of each term.
 * @param a - input param
 * @param size - input size
 * @return sum of exponents
 * \warning Function may overflow
 */
real expadd(real* a, int size) {
	real sum = 0;
	for (int i = 0; i < size; ++i)
		sum += exp(a[i]);
	return sum;
}
/**
 * Performs a softmax transformation on the input vector.
 * This function will not overflow for normal values of the input
 * @param output - output vector pointer 
 * @param input - input vector pointer 
 * @param input_size - input vector size 
 * @return softmax  values in \p output 
 */
void NNNer::softmax_layer(real* output, real* input, int input_size) {
	real sum = 0.0;
	sum = logadd(input, input_size);
	for (int i = 0; i < input_size; i++) {
		output[i] = exp(input[i] - sum);
	}
}
/** 
 * Search for index of maximum element in the input array
 * @param array - input array 
 * @param size - input size 
 * @return max element index in array
 */
int get_max_pos(real * array, int size) {
	real max = array[0];
	int max_pos = 0;

	int i;
	for (i = 1; i < size; i++) {
		if (max < array[i]) {
			max = array[i];
			max_pos = i;
		}
	}

	return max_pos;
}
/** 
 * Calculate the empirical transitition probabilities for the words 
 * @param training - training examples
 * @return  Initializes the \p viterbi_score_init and \p viterbi_score_trans with initial score and transitition scores respectively
 */ 
void NNNer::getEmpiricalTransitionProb(vector<Tokens>& training) {
	for (int i = 0; i < output_state_size * output_state_size; i++)
		viterbi_score_trans[i] = 1.0;
	for (int i = 0; i < output_state_size; i++)
		viterbi_score_init[i] = 1.0;
	long tsum = output_state_size * output_state_size, isum = output_state_size;
	for (int i = 0; i < training[0].num_words; i++) {
		viterbi_score_init[training[0].output_tag_idx[i]] += 1;
		isum += 1;
	}

	for (int i = 0; i < training.size(); i++) {
		for (int j = 1; j < training[i].num_words; j++) {
			viterbi_score_trans[IDXR(training[i].output_tag_idx[j-1],training[i].output_tag_idx[j],output_state_size)] +=
					1;
			tsum += 1;
		}
	}
	for (int i = 0; i < output_state_size; i++) {
		viterbi_score_init[i] /= (real) isum;
		for (int j = 0; j < output_state_size; j++) {
			viterbi_score_trans[IDXR(i,j,output_state_size)] /= (real) tsum;
		}
	}
}
/**
 * Max label decoding 
 * @param labels - output vector pointers for the labels 
 * @param input - output vector of the neural network
 * @param inputsize - input vector size
 * @param numwords - number of words to be decoded
 */
void maxlabels(int* labels, real* input, int inputsize, int numwords) {
	for (int i = 0; i < numwords; i++) {
		labels[i] = get_max_pos(input + i * inputsize, inputsize);
	}
}
/**
 * Function to perform forward pass on bunch of data set
 * @param training_data - input sentences as training examples
 * @return Initializes the \p sys_tag_idx in \p training_data to appropriate indexes
 */
void NNNer::test(vector<Tokens>& training_data) {

	for (int i = 0; i < training_data.size(); i++) {
		if (training_data[i].num_words == 0) {
			print_message("parsing error");
			continue;
		}
		input_state = (real*) reallocMemory(input_state, sizeof(real),
				input_state_size
						* (training_data[i].num_words + window_size - 1));
		hidden_state = (real*) reallocMemory(hidden_state, sizeof(real),
				hidden_state_size * training_data[i].num_words);
		output_state = (real*) reallocMemory(output_state, sizeof(real),
				output_state_size * training_data[i].num_words);
		forwardPass(training_data[i]);
		training_data[i].sys_tag_idx = (int*) reallocMemory(training_data[i].sys_tag_idx,sizeof(int), training_data[i].num_words);
		maxlabels(training_data[i].sys_tag_idx, output_state, output_state_size,
				training_data[i].num_words);
//		viterbi_inference(labels, viterbi_score_init, viterbi_score_trans,
//				output_state, output_state_size, training_data[i].num_words);

	}
}
/**
 * Function to perform forward pass on single sentence
 * @param training_data - input sentence as training examples
 * @return Initializes the \p sys_tag_idx in \p training_data to appropriate indexes
 */
void NNNer::test(Tokens& training_data) {
	if (training_data.num_words == 0) {
		print_message("parsing error");
		return;
	}
	input_state = (real*) reallocMemory(input_state, sizeof(real),
					input_state_size
							* (training_data.num_words + window_size - 1));
	hidden_state = (real*) reallocMemory(hidden_state, sizeof(real),
					hidden_state_size * training_data.num_words);
	output_state = (real*) reallocMemory(output_state, sizeof(real),
					output_state_size * training_data.num_words);
	forwardPass(training_data);
	training_data.sys_tag_idx = (int*) reallocMemory(training_data.sys_tag_idx,sizeof(int), training_data.num_words);
	maxlabels(training_data.sys_tag_idx, output_state, output_state_size,
			training_data.num_words);
}
/**
 * Complement function to nnlookup. This function updates the feature vectors.
 * @param lookupTable - pointer  to lookup table to be updated
 * @param stride - skip size for input vectors
 * @param delta - pointer to derivative vector
 * @param vecSize - derivative vector size
 * @param maxidx - vocab max id
 * @param featureIndices - feature vector
 * @param numwords - number of words
 * @param padidx - padding index of the feature
 * @param npad - number of pads applied
 *
 */
void NNNer::updateFeatures(real* lookupTable, int stride, const real* delta,
		int vecSize, int maxidx, const int* featureIndices, int numWords,
		int padidx, int npad) {
	int i;
	int* count = new int[numWords + window_size - 1]();
	int windowSize = npad * 2 + 1;

	for (i = npad; i <= npad+numWords; i++) {
		for(int j=i-npad;j<i+npad;j++){
			count[j]+=1;
		}
	}
	if (padidx < 0 || padidx >= maxidx)
		print_error("lookup: padding index out of range");
	real* padVec = &lookupTable[IDXR(padidx,0,vecSize)];
	for (i = 0; i < npad; ++i) {
		for (int j = 0; j < vecSize; j++) {
			padVec[j] += (lr * delta[i * stride + j] / (real) count[i]);
		}
	}
	for (i = 0; i < numWords; ++i) {
		int fidx = featureIndices[i];
		if (fidx < 0 || fidx >= maxidx)
			print_error("lookup: wordIdx out of range");
		real* fvec = &lookupTable[IDXR(fidx,0,vecSize)];
		for (int j = 0; j < vecSize; j++) {
			fvec[j] += (lr * delta[(i + npad) * stride + j]
					/ (real) count[i + npad]);
		}
	}
	for (i = 0; i < npad; ++i) {
		for (int j = 0; j < vecSize; j++) {
			padVec[j] += (lr * delta[(i + npad + numWords) * stride + j]
					/ (real) count[i + npad + numWords]);
		}
	}

	delete[] count;
}
/**
 * Core function to do training of neural network
 * @param  training_data - training data
 * @param validation_data - validation data
 * @return Updates network paramters
 */
void NNNer::learnNet(vector<Tokens>& training_data,vector<Tokens>& validation_data) {
	int training_size = training_data.size();
	real damp_factor = 1.0;
	real* deltaTrans = (real*) allocMemory(sizeof(real),
			output_state_size * output_state_size);
	real* deltaOutput = NULL;
	real* phi = NULL;
	real* interm = (real*) allocMemory(sizeof(real), output_state_size);
	real* delta = (real*) allocMemory(sizeof(real), output_state_size);
	real* deltap = (real*) allocMemory(sizeof(real), output_state_size);
	real* sum1 = (real*) allocMemory(sizeof(real), output_state_size);
	real* hidden_layer_delta = (real*) allocMemory(sizeof(real),
			hidden_state_size);
	real* output_layer_error = (real *) allocMemory(sizeof(real),
			hidden_state_size * output_state_size);
	real* delta_viterbi_init = (real *) allocMemory(sizeof(real),
			output_state_size);
	initNet();


//	capFeature->reset();
//	gazlFeature->reset();
//	gazpFeature->reset();
//	gazmFeature->reset();
//	gazpFeature->reset();
	int corpus_word_count=countWords(training_data);
	int train_word_count=0;
	int epoch = 0;
	/* Print stats */
	printf("Number of sentences:\t%d\n",(int)training_data.size());
	printf("Total number of words:\t%d\n",corpus_word_count);
	printf("Initial learning rate:\t%f\n",lr);

	while (epoch < max_epoch) {
		epoch++;
		getEmpiricalTransitionProb(training_data);
		int i = 0;
		real oerror=0.0;
		for (int k = 0; k < training_size; k++) { // For each training example
			//Reset the network with the input sentence size
			i = k;

			//training_data[i].print();
			input_state = (real*) reallocMemory(input_state, sizeof(real),
					input_state_size
							* (training_data[i].num_words + window_size - 1));
			output_state = (real*) reallocMemory(output_state, sizeof(real),
					output_state_size * training_data[i].num_words);
			hidden_state = (real*) reallocMemory(hidden_state, sizeof(real),
					hidden_state_size * training_data[i].num_words);
			deltaOutput = (real*) reallocMemory(deltaOutput, sizeof(real),
					training_data[i].num_words * output_state_size);
			phi = (real*) reallocMemory(phi, sizeof(real),
					output_state_size * training_data[i].num_words);
			memset(phi, 0,
					output_state_size * training_data[i].num_words
							* sizeof(real));
			memset(input_state, 0,
					sizeof(real)
							* (training_data[i].num_words + window_size - 1)
							* input_state_size);
			memset(hidden_state, 0,
					sizeof(real) * hidden_state_size
							* training_data[i].num_words);
			memset(output_state, 0,
					output_state_size * training_data[i].num_words
							* sizeof(real));
			memset(deltaTrans, 0,
					output_state_size * output_state_size * sizeof(real));
			memset(deltaOutput, 0,
					output_state_size * training_data[i].num_words
							* sizeof(real));
			memset(hidden_layer_delta, 0, sizeof(real) * hidden_state_size);
			memset(deltap, 0, sizeof(real) * output_state_size);
			memset(delta, 0, sizeof(real) * output_state_size);
			memset(delta_viterbi_init, 0, sizeof(real) * output_state_size);

			//forward pass
			forwardPass(training_data[i]);

//		//Calculation for derivatives  in network params
			real* trg = new real[output_state_size];
			real* deltaInput = new real[input_state_size
					* (training_data[i].num_words + window_size - 1)];
			memset(deltaInput, 0,
					sizeof(real) * input_state_size
							* (training_data[i].num_words + window_size - 1));
			real error=0;
			for (int j = 0; j < training_data[i].num_words; j++) {
				train_word_count++;
				//Calculate derivatives

				memset(trg, 0, output_state_size * sizeof(real));
				trg[training_data[i].output_tag_idx[j]] = 1.0;
				for (int k = 0; k < output_state_size; k++) {
					output_layer_error[k] = (trg[k]
							- output_state[IDXR(j,k,output_state_size)])//deltaOutput[IDXR(j,k,output_state_size)]
					* dsigmoid(output_state[IDXR(j,k,output_state_size)]);
					error+=0.5*(trg[k]-output_state[IDXR(j,k,output_state_size)])*(trg[k]-output_state[IDXR(j,k,output_state_size)]);
					oerror+=error;
				}
				for (int k = 0; k < hidden_state_size; k++) {
					hidden_layer_delta[k] = 0.0;
					for (int l = 0; l < output_state_size; l++) {
						hidden_layer_delta[k] += (output_layer_error[l]
								* l2_weight[IDXR(l,k,hidden_state_size)]);
					}
					hidden_layer_delta[k] *= dsigmoid(
							hidden_state[IDXR(j,k,hidden_state_size)]);
				}
				for (int k = 0; k < hidden_state_size; k++) {
					for (int l = 0; l < input_state_size * window_size; l++) {
						deltaInput[j * input_state_size + l] +=
								hidden_layer_delta[k]
										* l1_weight[IDXR(k,l,input_state_size*window_size)];
					}
				}
				for (int k = 0; k < hidden_state_size; k++) {
					for (int l = 0; l < input_state_size * window_size; l++) {
						l1_weight[IDXR(k,l,(input_state_size*window_size))] += (
								alpha*prev_l1_weight[IDXR(k,l,(input_state_size*window_size))]);
					}
					l1_bias[k] += (alpha * prev_l1_bias[k]);
				}
				for (int k = 0; k < hidden_state_size; k++) {
					for (int l = 0; l < input_state_size * window_size; l++) {
						prev_l1_weight[IDXR(k,l,input_state_size*window_size)] =
								(lr * hidden_layer_delta[k]
										* input_state[IDXR(j,l,input_state_size)]);
						l1_weight[IDXR(k,l,(input_state_size*window_size))] +=
								prev_l1_weight[IDXR(k,l,input_state_size*window_size)];
					}
					prev_l1_bias[k] = (lr * hidden_layer_delta[k]);
					l1_bias[k] += prev_l1_bias[k];
				}
				for (int k = 0; k < output_state_size; k++) {
					for (int l = 0; l < hidden_state_size; l++) {
						l2_weight[IDXR(k,l,hidden_state_size)] +=(alpha*prev_l2_weight[IDXR(k,l,hidden_state_size)]);
					}
					l2_bias[k] += (alpha*prev_l2_bias[k]);
				}
				for (int k = 0; k < output_state_size; k++) {
					for (int l = 0; l < hidden_state_size; l++) {
						prev_l2_weight[IDXR(k,l,hidden_state_size)] = (lr
								* output_layer_error[k]
								* hidden_state[IDXR(j,l,hidden_state_size)]);
						l2_weight[IDXR(k,l,hidden_state_size)] += prev_l2_weight[IDXR(k,l,hidden_state_size)];
					}
					prev_l2_bias[k] = (lr * output_layer_error[k]);
					l2_bias[k] += prev_l2_bias[k] ;
				}

			}
			int psize = wordVecSize;
			if (capFeature) {
				updateFeatures(capFeature->lookup_table, input_state_size,
						&deltaInput[psize], capFeature->vecSize,
						capFeature->featureHash->size(),
						training_data[i].cap_feature,
						training_data[i].num_words,
						capFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize +=capFeature->vecSize;
			}
			if (gazpFeature) {
				updateFeatures(gazpFeature->lookup_table, input_state_size,
						&deltaInput[psize], gazpFeature->vecSize,
						gazpFeature->featureHash->size(),
						training_data[i].gazp_idx, training_data[i].num_words,
						gazpFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize += gazpFeature->vecSize;
			}
			if (gazlFeature) {
				updateFeatures(gazlFeature->lookup_table, input_state_size,
						&deltaInput[psize], gazlFeature->vecSize,
						gazlFeature->featureHash->size(),
						training_data[i].gazl_idx, training_data[i].num_words,
						gazlFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);\
				psize += gazlFeature->vecSize;
			}
			if (gazoFeature) {
				updateFeatures(gazoFeature->lookup_table, input_state_size,
						&deltaInput[psize], gazoFeature->vecSize,
						gazoFeature->featureHash->size(),
						training_data[i].gazo_idx, training_data[i].num_words,
						gazoFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize += gazoFeature->vecSize;
			}
			if (gazmFeature) {
				updateFeatures(gazmFeature->lookup_table, input_state_size,
						&deltaInput[psize], gazmFeature->vecSize,
						gazmFeature->featureHash->size(),
						training_data[i].gazm_idx, training_data[i].num_words,
						gazmFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize += gazmFeature->vecSize;
			}
			if (bdllFeature) {
				updateFeatures(bdllFeature->lookup_table, input_state_size,
						&deltaInput[psize], bdllFeature->vecSize,
						bdllFeature->featureHash->size(),
						training_data[i].bdll_idx, training_data[i].num_words,
						bdllFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize += bdllFeature->vecSize;
			}
			if (diceFeature) {
				updateFeatures(diceFeature->lookup_table, input_state_size,
						&deltaInput[psize], diceFeature->vecSize,
						diceFeature->featureHash->size(),
						training_data[i].dice_idx, training_data[i].num_words,
						diceFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize += diceFeature->vecSize;
			}
			if (npmiFeature) {
				updateFeatures(npmiFeature->lookup_table, input_state_size,
						&deltaInput[psize], npmiFeature->vecSize,
						npmiFeature->featureHash->size(),
						training_data[i].npmi_idx, training_data[i].num_words,
						npmiFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize += npmiFeature->vecSize;
			}
			if (entropyFeature) {
				updateFeatures(entropyFeature->lookup_table, input_state_size,
						&deltaInput[psize], entropyFeature->vecSize,
						entropyFeature->featureHash->size(),
						training_data[i].entropy_idx, training_data[i].num_words,
						entropyFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
				psize += entropyFeature->vecSize;
			}
			if (resIdfFeature) {
					updateFeatures(resIdfFeature->lookup_table, input_state_size,
						&deltaInput[psize], resIdfFeature->vecSize,
						resIdfFeature->featureHash->size(),
						training_data[i].residf_idx, training_data[i].num_words,
						resIdfFeature->featureHash->getIndex("PADDING"),
						(window_size - 1) / 2);
			 	psize += resIdfFeature->vecSize;
			}
			delete[] trg;
			delete[] deltaInput;


			lr = starting_lr * (1 - (epoch*corpus_word_count+train_word_count) / (real)(max_epoch*corpus_word_count + 1));
			if (lr < starting_lr * 0.0001) lr = starting_lr * 0.0001;
			if (k % 100 == 0){
			//	lr = updateLearningRate(validation_data,lr);
				printf("\rtraining sentence %d Error:%f", k,error/training_data[i].num_words);
				fflush(stdout);
			}

//		printf("After update:\n");
//		forwardPass(training_data[i]);
//		print_vector_2d(output_state, training_data[i].num_words,
//				output_state_size);

		}

//	print_vector_2d(l1_weight,hidden_state_size,input_state_size*window_size);
//	print_vector_1d(l1_bias,hidden_state_size);
//	print_vector_2d(l2_weight,output_state_size,hidden_state_size);
//	print_vector_1d(l2_bias,output_state_size);
//	print_vector_1d(viterbi_score_init,output_state_size);
//	print_vector_2d(viterbi_score_trans,output_state_size,output_state_size);
		printf("%c[2K\rEpoch complete: %d\t learning rate:%f\t Overall Error:%f\n", 27, epoch,lr,(oerror/train_word_count));
		//Validate
		validate(validation_data,epoch);
		train_word_count=0;
		sprintf(paramFile,"%s/param_%d.mod",outputPath,epoch);
		storeNNet();
	}

	free(deltaTrans);
	free(deltaOutput);
	free(deltap);
	free(delta);
	free(interm);
	free(phi);
	free(output_layer_error);
	free(sum1);
	free(hidden_layer_delta);
	free(delta_viterbi_init);
}
/**
 * Dynamic update rule for the learning rate based on error on validation data
 * @param val_data - validation data
 * @param lr - current learning rate
 * @return updated learning rate
 */
real NNNer::updateLearningRate(vector<Tokens>& val_data,real lr){
	int train_word_count=0;
	real cost=0;
	real* output_layer_error = (real*) allocMemory(sizeof(real),output_state_size);
	for (int i = 0; i < val_data.size(); i++) {
		if (val_data[i].num_words == 0) {
			print_message("parsing error");
			continue;
		}
		input_state = (real*) reallocMemory(input_state, sizeof(real),
				input_state_size
						* (val_data[i].num_words + window_size - 1));
		hidden_state = (real*) reallocMemory(hidden_state, sizeof(real),
				hidden_state_size * val_data[i].num_words);
		output_state = (real*) reallocMemory(output_state, sizeof(real),
				output_state_size * val_data[i].num_words);
		forwardPass(val_data[i]);

	//		viterbi_inference(labels, viterbi_score_init, viterbi_score_trans,
	//				output_state, output_state_size, training_data[i].num_words);
		real* trg = new real[output_state_size];

		for (int j = 0; j < val_data[i].num_words; j++) {
			train_word_count++;
						//Calculate derivatives

			memset(trg, 0, output_state_size * sizeof(real));
			trg[val_data[i].output_tag_idx[j]] = 1.0;
			for (int k = 0; k < output_state_size; k++) {
			output_layer_error[k] = (trg[k]
						- output_state[IDXR(j,k,output_state_size)])//deltaOutput[IDXR(j,k,output_state_size)]
						* dsigmoid(output_state[IDXR(j,k,output_state_size)]);
						cost+=0.5*(trg[k]-output_state[IDXR(j,k,output_state_size)])*(trg[k]-output_state[IDXR(j,k,output_state_size)]);
			}
		}
		free(trg);
	}
	cost=cost/train_word_count;
	free(output_layer_error);
	if((cost-prev_cost)/prev_cost>0.1){

	}
	if(cost>prev_cost)
		lr =lr*0.98;
	prev_cost =cost;
	return lr;
}
/**
 * Accuracy checking code
 * @param data - training data
 * @param epoch - current epoch id
 * \remark Needs conlleval.pl to calculate precision and recall
 */
void NNNer::validate(vector<Tokens>& data,int epoch){
	test(data);
	char filename[200];
	char command[300];
	sprintf(filename,"%s/test_%d.txt",outputPath,epoch);
	sprintf(command,"perl ~/conlleval.pl -r -d '\t' < %s/test_%d.txt",outputPath,epoch);
	char buf[128];
	FILE * fp = safe_fopen(filename,"w");
	for(int i=0;i<data.size();i++){
		data[i].writeData(fp);
		fprintf(fp,"\n");
	}
	safe_fclose(fp);
	FILE* console;

	console = popen(command,"r");
	if(!console) print_error("accuracy check unsuccessful");
	while(fgets(buf,sizeof(buf),console)!=NULL){
		printf("%s",buf);
	}
	printf("\n");
	fclose(console);

}
/**
 * Viterbi inference algorithm
 * @param path - max score path holder
 * @param init - init scores for viterbi
 * @param transition - transition score for trellis
 * @param emission - emission probability scores of the trellis
 * @param N - number of words in the sentence
 * @param T - number of tags
 * @return - Populates the path with max score path
 */
void NNNer::viterbi_inference(int* path, real* init, real* transition,
		real* emission, int N, int T) {
	real *delta, *deltap;
	int *phi;
	int i, j, t;


	/* misc allocations */
	delta = (real*) allocMemory(sizeof(real), N);
	deltap = (real*) allocMemory(sizeof(real), N);
	phi = (int*) allocMemory(sizeof(int), N * T);

	/* init */
	for (i = 0; i < N; i++)
		deltap[i] = log(init[i]) + log(emission[i]);

	/* recursion */
	for (t = 1; t < T; t++) {
		real *deltan = delta;
		for (j = 0; j < N; j++) {
			real maxValue = -std::numeric_limits<real>::max();
			int maxIndex = 0;
			for (i = 0; i < N; i++) {
				real z = deltap[i] + log(transition[IDXR(j,i,N)]);
				if (z > maxValue) {
					maxValue = z;
					maxIndex = i;
				}
			}
			delta[j] = maxValue + log(emission[IDXR(t,j,N)]);
			phi[IDXR(t,j,N)] = maxIndex;
		}
		delta = deltap;
		deltap = deltan;
	}

	{
		real maxValue = -std::numeric_limits<real>::max();
		int maxIndex = 0;
		for (j = 0; j < N; j++) {
			if (deltap[j] > maxValue) {
				maxValue = deltap[j];
				maxIndex = j;
			}
		}
		path[T - 1] = maxIndex;
	}

	for (t = T - 2; t >= 0; t--)
		path[t] = phi[path[t + 1] + (t + 1) * N];

	free(delta);
	free(deltap);
	free(phi);
}
/**
 * Derivative of hardtanh
 * @param x - input value
 * @return derivative value
 */
real NNNer::dtanh(real x) {
	if (x < -1 || x > 1)
		return 0.0;
	return 1.0;
}
/**
 * Derivative of softmax
 * @param x - input value
 * @return derivative value
 */
real NNNer::dsoftmax(real x) {
	return x * (1 - x);
}
/**
 * perform sigmoid transformation
 * @param x - input value
 * @return sigmoid of x
 */
real NNNer::sigmoid(real x){
	return 1/(1+exp(-x));
}
/**
 * Derivative of sigmoid
 * @param x - input value
 * @return derivative value
 */
real NNNer::dsigmoid(real x){
	return x*(1-x);
}
/**
 * Count total number of words in the training set
 * @param t - vector of input sentences
 * @return count of words
 */
int NNNer::countWords(vector<Tokens>& t){
	int count=0;
	for(int i=0;i<t.size();i++){
		count += t[i].num_words;
	}
	return count;
}
