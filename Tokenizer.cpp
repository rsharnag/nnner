/*
 * Tokenizer.cpp
 *
 *  Created on: 16-May-2014
 *      Author: cfilt
 */
#include<iostream>
using namespace std;
#include "Tokenizer.h"
char *trimwhitespace(char *str);
Tokenizer::~Tokenizer() {
}
string Tokenizer::breakWord(string s){
	int pos = 0, lastPos=0;
	while((pos=s.find_first_of("',.?><()[]{}\"/`!@#$%^&*",lastPos))!=string::npos){
		s.insert(pos," ");
		//sresult.push_back(s.substr(pos,pos+1));
		lastPos=pos+2;
	}
	return s;
}
void Tokenizer::tokenize(vector<Tokens>& toks) {
	for (int i = 0; i < toks.size(); i++) {
		for (int j = 0; j < toks[i].num_words; j++) {
			toks[i].word_idx[j] = word_hash->getIndex(toks[i].words[j]);
		}
	}
}
Tokens Tokenizer::tokenize_sent_format(string& sent){
	sent = breakWord(sent);
	cout<<sent<<endl;
	istringstream iss(sent);
	vector<string> words;
	do{
		string sub="";
		iss >> sub;
		if(iss.eof() && sub.size()==0) break;
		if(sub=="") continue;
		words.push_back(sub);
	}while(!iss.eof());

	Tokens tokens(words.size());
	for(int i=0;i<words.size();i++){
		tokens.words[i]= (char*) allocMemory(sizeof(char),words[i].size()+1);


		strncpy(tokens.words[i],words[i].c_str(), words[i].size());
		// tokens.tags = tags; //Not needed for testing

		tokens.tagHash = ner_hash;

		if(cap_hash) tokens.cap_feature[i] =checkCap(words[i].c_str(),words[i].size());
		int k=0;
		//lower case the word
		while(k<words[i].size()) {words[i][k]=tolower(words[i][k]);k++;}
			tokens.word_idx[i] =(word_hash->getIndex(words[i].c_str()) == -1) ?
					word_hash->getIndex("UNKNOWN") :
					word_hash->getIndex(words[i].c_str());

	}

	if(gazp_hash){
		gazetteer_tokenizer(tokens,tokens.words,gazp_hash,gazt_hash,tokens.gazp_idx);}
	if(gazl_hash){
		gazetteer_tokenizer(tokens,tokens.words,gazl_hash,gazt_hash,tokens.gazl_idx);}
	if(gazm_hash){
		gazetteer_tokenizer(tokens,tokens.words,gazm_hash,gazt_hash,tokens.gazm_idx);}
	if(gazo_hash){
		gazetteer_tokenizer(tokens,tokens.words,gazo_hash,gazt_hash,tokens.gazo_idx);}
	if(bdll_hash){
		gazetteer_tokenizer(tokens,tokens.words,bdll_hash,gazt_hash,tokens.bdll_idx);}
	if(dice_hash){
		gazetteer_tokenizer(tokens,tokens.words,dice_hash,gazt_hash,tokens.dice_idx);}
	if(npmi_hash){
		gazetteer_tokenizer(tokens,tokens.words,npmi_hash,gazt_hash,tokens.npmi_idx);}
	if(residf_hash){
		gazetteer_tokenizer(tokens,tokens.words,residf_hash,gazt_hash,tokens.residf_idx);}
	if(entropy_hash){
		gazetteer_tokenizer(tokens,tokens.words,entropy_hash,gazt_hash,tokens.entropy_idx);}

	return tokens;

}
void Tokenizer::tokenize_column_format(const char* filename,
		vector<Tokens>& toks) {
	FILE* input = safe_fopen(filename, "r");
	//Assumes last tag is the output tag
	char line[MAXLINE] = { 0 };
	int lineCount=0;
	int tokenCount = 0;
	int tagCount = 0;
	char* out = fgets(line, MAXLINE, input);
	lineCount++;
	int i = 0;
	char** words = (char**) allocMemory(sizeof(char*), max_tokens);
	char*** tags = (char***) allocMemory(sizeof(char**), max_tokens);
	int* word_idx = (int*) allocMemory(sizeof(int), max_tokens);
	int* output_tag_idx = (int*) allocMemory(sizeof(int), max_tokens);
	int* capFeature =NULL;
	int* gazp_idx = NULL;
	int* gazl_idx = NULL;
	int* gazo_idx = NULL;
	int* gazm_idx = NULL;
	int* bdll_idx =NULL;
	int* dice_idx =NULL;
	int* entropy_idx =NULL;
	int* npmi_idx = NULL;
	int* residf_idx =NULL;
	if(cap_hash)
		capFeature =(int*) allocMemory(sizeof(int),max_tokens);
	if(gazp_hash)
		gazp_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(gazo_hash)
		gazo_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(gazl_hash)
		gazl_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(gazm_hash)
		gazm_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(bdll_hash)
			bdll_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(dice_hash)
			dice_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(entropy_hash)
			entropy_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(npmi_hash)
			npmi_idx = (int*) allocMemory(sizeof(int),max_tokens);
	if(residf_hash)
		residf_idx =  (int*) allocMemory(sizeof(int),max_tokens);
	while (strlen(line) > 0 && line[i] != '\n') {
		if (isspace(line[i]))
			tagCount++;
		i++;
	}

	while (out != NULL) {
		if (tokenCount + 2 > max_tokens) { // max limit for sentence almost reached
			max_tokens += 5;
			words = (char**) reallocMemory(words, sizeof(char*), max_tokens);
			tags = (char***) reallocMemory(tags, sizeof(char**), max_tokens);
			word_idx = (int*) reallocMemory(word_idx, sizeof(int), max_tokens);
			output_tag_idx = (int*) reallocMemory(output_tag_idx, sizeof(int),
					max_tokens);
			if(cap_hash)
				capFeature = (int*) reallocMemory(capFeature,sizeof(int),max_tokens);
			if(gazp_hash)
				gazp_idx = (int*) reallocMemory(gazp_idx,sizeof(int),max_tokens);
			if(gazo_hash)
				gazo_idx = (int*) reallocMemory(gazo_idx,sizeof(int),max_tokens);
			if(gazl_hash)
				gazl_idx = (int*) reallocMemory(gazl_idx,sizeof(int),max_tokens);
			if(gazm_hash)
				gazm_idx = (int*) reallocMemory(gazm_idx,sizeof(int),max_tokens);
			if(bdll_hash)
				bdll_idx = (int*) reallocMemory(bdll_idx,sizeof(int),max_tokens);
			if(dice_hash)
				dice_idx = (int*) reallocMemory(dice_idx,sizeof(int),max_tokens);
			if(entropy_hash)
				entropy_idx = (int*) reallocMemory(entropy_idx,sizeof(int),max_tokens);
			if(npmi_hash)
				npmi_idx = (int*) reallocMemory(npmi_idx,sizeof(int),max_tokens);
			if(residf_hash)
				residf_idx = (int*) reallocMemory(residf_idx,sizeof(int),max_tokens);

		}
		if (strcmp(trimwhitespace(line), "\n") == 0 || strcmp(trimwhitespace(line), "") == 0) {
			if(tokenCount==0){
				out = fgets(line, MAXLINE, input);
				lineCount++;
				continue;
			}
			Tokens tokens;
			tokens.num_words = tokenCount;
			tokens.words = words;
			tokens.tags = tags;
			tokens.tagHash = ner_hash;
			tokens.tag_count = tagCount;
			tokens.output_tag_idx = output_tag_idx;
			tokens.word_idx = word_idx;
			tokens.cap_feature=capFeature;
			if(gazp_hash){tokens.gazp_idx=gazp_idx;gazetteer_tokenizer(tokens,tokens.words,gazp_hash,gazt_hash,tokens.gazp_idx);}
			if(gazl_hash){tokens.gazl_idx=gazl_idx;gazetteer_tokenizer(tokens,tokens.words,gazl_hash,gazt_hash,tokens.gazl_idx);}
			if(gazm_hash){tokens.gazm_idx=gazm_idx;gazetteer_tokenizer(tokens,tokens.words,gazm_hash,gazt_hash,tokens.gazm_idx);}
			if(gazo_hash){tokens.gazo_idx=gazo_idx;gazetteer_tokenizer(tokens,tokens.words,gazo_hash,gazt_hash,tokens.gazo_idx);}
			if(bdll_hash){tokens.bdll_idx=bdll_idx;gazetteer_tokenizer(tokens,tokens.words,bdll_hash,gazt_hash,tokens.bdll_idx);}
			if(dice_hash){tokens.dice_idx=dice_idx;gazetteer_tokenizer(tokens,tokens.words,dice_hash,gazt_hash,tokens.dice_idx);}
			if(npmi_hash){tokens.npmi_idx=npmi_idx;gazetteer_tokenizer(tokens,tokens.words,npmi_hash,gazt_hash,tokens.npmi_idx);}
			if(residf_hash){tokens.residf_idx=npmi_idx;gazetteer_tokenizer(tokens,tokens.words,residf_hash,gazt_hash,tokens.residf_idx);}
			if(entropy_hash){tokens.entropy_idx=entropy_idx;gazetteer_tokenizer(tokens,tokens.words,entropy_hash,gazt_hash,tokens.entropy_idx);}
			toks.push_back(tokens);

			tokenCount = 0;
			max_tokens = 30;
			words = (char**) allocMemory(sizeof(char*), max_tokens);
			tags = (char***) allocMemory(sizeof(char**), max_tokens);
			word_idx = (int*) allocMemory(sizeof(int), max_tokens);
			output_tag_idx = (int*) allocMemory(sizeof(int), max_tokens);
			if(cap_hash) capFeature = (int*) allocMemory(sizeof(int),max_tokens);
			if(gazp_hash) gazp_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(gazo_hash) gazo_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(gazl_hash) gazl_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(gazm_hash) gazm_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(bdll_hash) bdll_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(dice_hash) dice_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(entropy_hash) entropy_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(npmi_hash) npmi_idx = (int*) allocMemory(sizeof(int),max_tokens);
			if(residf_hash)	residf_idx =  (int*) allocMemory(sizeof(int),max_tokens);
 			out = fgets(line, MAXLINE, input);
 			lineCount++;
			if (!out) {
				free(words);
				free(tags);
				free(output_tag_idx);
				free(word_idx);
				free(capFeature);
				free(gazl_idx);
				free(gazo_idx);
				free(gazm_idx);
				free(gazp_idx);
				free(bdll_idx);
				free(dice_idx);
				free(npmi_idx);
				free(entropy_idx);
				free(residf_idx);
			}
			continue;
		}
		char* word = (char*) allocMemory(sizeof(char), MAXSTRING);
		char** tag = (char**) allocMemory(sizeof(char*), tagCount);
		for (i = 0; i < tagCount; ++i) {
			tag[i] = (char*) allocMemory(sizeof(char), MAXSTRING);
			memset(tag[i], 0, MAXSTRING * sizeof(char));
		}
		memset(word, 0, MAXSTRING * sizeof(char));
		int roffset = 0;
		int woffset = 0;
		int tag_index = 0;
		while (roffset < MAXLINE) {
			if (line[roffset] == '\n' || line[roffset] == '\0') //EOL
				break;
			while (isspace(line[roffset])) //Skip spaces
				roffset++;
			if (tag_index == 0) {
				while (woffset < MAXSTRING && !isspace(line[roffset]))
					word[woffset++] = line[roffset++];
			}
			if(woffset==MAXSTRING) while(!isspace(line[roffset++]));
			while (isspace(line[roffset])) //Skip spaces
				roffset++;
			woffset = 0;
			while (woffset < MAXSTRING && !isspace(line[roffset]) && tag_index<tagCount && line[roffset]!='\0')
				tag[tag_index][woffset++] = line[roffset++];
			if(tag_index>=tagCount) print_message("Error : %s\t%d\n",line,lineCount);
			tag_index++;

		}
		//Processing on word
		if(cap_hash){
			capFeature[tokenCount] = checkCap(word,strlen(word)+1);
			for (int a = 0; a < strlen(word); a++) {
						word[a] = tolower(word[a]);
			}
		}
		word_idx[tokenCount] =
				(word_hash->getIndex(word) == -1) ?
						word_hash->getIndex("UNKNOWN") :
						word_hash->getIndex(word);


		output_tag_idx[tokenCount] =
				(ner_hash->getIndex(tag[tagCount - 1]) == -1) ?
						ner_hash->getIndex("O") :
						ner_hash->getIndex(tag[tagCount - 1]);
		words[tokenCount] = (char*) reallocMemory(word, sizeof(char),
				strlen(word) + 1);
		tags[tokenCount] = tag;
		tokenCount++;
		out = fgets(line, MAXLINE, input);
		lineCount++;
	}

	safe_fclose(input);

}
int Tokenizer::checkCap(const char* w, int size){
	int retVal=cap_hash->getIndex("NC");
	bool allCap=false,hasCap=false,initCap=false;
	allCap = !islower(w[0]);
	hasCap = initCap =isupper(w[0]);

	for(int i=1;i<size;i++){
		if(islower(w[i])) allCap =false;
		else if(isupper(w[i])) hasCap = true;
	}
	if(hasCap && allCap)
		retVal = cap_hash->getIndex("ALLCAPS");
	else if(initCap)
		retVal = cap_hash->getIndex("INITCAP");
	else if(hasCap)
		retVal = cap_hash->getIndex("HASCAP");
	return retVal;
}
void Tokenizer::gazetteer_tokenizer(Tokens& toks,char** tokens,Hash* gaz_hash,Hash* gazt_hash,int * tags){
	int maxLength = MAX_STRING;
	char* word=(char*)allocMemory(sizeof(char),maxLength);
	int wordLen =0;
	int hash_idx;
	char** words = toks.words;
	memset (word,0,sizeof(char)*MAX_STRING);
	int no = gazt_hash->getIndex("NO");
	int yes = gazt_hash->getIndex("YES");
	for(int i=0;i<toks.num_words;i++){
		tags[i]=no;
	}
	for(int i=0;i<toks.num_words;i++){
		wordLen = 0;
		for(int j=0;j<toks.num_words-i;j++){
			int s = strlen(words[i+j]);
			if((s+wordLen+1)>maxLength){
				word =(char*) reallocMemory(word,sizeof(char),wordLen+s+1);
				maxLength = wordLen+s+1;
			}
			if(j>0)
				word[wordLen-1]=' ';
			for(int k=0;k<s;k++){
				word[wordLen++] = (char)tolower(words[i+j][k]);
			}
			word[wordLen++]='\0';
			hash_idx = gaz_hash->getIndex(word);
			if(hash_idx<0){
				break;
			}
			for(int k=0;k<=j;k++){
				tags[i+k]= yes;
			}
		}
	}
	free(word);
}
char *trimwhitespace(char *str)
{
  char *end;

  // Trim leading space
  while(isspace(*str)) str++;

  if(*str == 0)  // All spaces?
    return str;

  // Trim trailing space
  end = str + strlen(str) - 1;
  while(end > str && isspace(*end)) end--;

  // Write new null terminator
  *(end+1) = 0;

  return str;
}
