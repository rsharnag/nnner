var searchData=
[
  ['rand',['rand',['../utils_8cpp.html#ae89ecdb4663673e009c78ccd095a667d',1,'rand(size_t m, size_t n):&#160;utils.cpp'],['../utils_8h.html#ae89ecdb4663673e009c78ccd095a667d',1,'rand(size_t m, size_t n):&#160;utils.cpp']]],
  ['randn',['randn',['../utils_8cpp.html#a50980f2f035c2dd59543cdc39a68cea2',1,'randn(size_t m, size_t n):&#160;utils.cpp'],['../utils_8h.html#a50980f2f035c2dd59543cdc39a68cea2',1,'randn(size_t m, size_t n):&#160;utils.cpp']]],
  ['random',['random',['../classNNNer.html#a238d46a1f1ee5dd0b2ee89453b74578f',1,'NNNer']]],
  ['randrange',['randRange',['../utils_8cpp.html#a91cfbd41b83abd9763d8293e37300629',1,'randRange(size_t m, size_t n, real a, real b):&#160;utils.cpp'],['../utils_8h.html#a91cfbd41b83abd9763d8293e37300629',1,'randRange(size_t m, size_t n, real a, real b):&#160;utils.cpp']]],
  ['read_5fvector_5f1d',['read_vector_1d',['../utils_8cpp.html#a56d6f9c9dcb527731320aba3056e2630',1,'read_vector_1d(real **vec, int *vecSize, FILE *f, bool binary):&#160;utils.cpp'],['../utils_8h.html#a4480eeabd59b30642a9009025f0e3c32',1,'read_vector_1d(real **vec, int *row, int *col, FILE *f, bool binary):&#160;utils.h']]],
  ['read_5fvector_5f2d',['read_vector_2d',['../utils_8cpp.html#ac53d28572029e531797680dc9d250b16',1,'read_vector_2d(real **vec, int *row, int *col, FILE *f, bool binary):&#160;utils.cpp'],['../utils_8h.html#ac53d28572029e531797680dc9d250b16',1,'read_vector_2d(real **vec, int *row, int *col, FILE *f, bool binary):&#160;utils.cpp']]],
  ['reallocmemory',['reallocMemory',['../utils_8cpp.html#a3fa303c494e2499ac5c03d2cf5f190b5',1,'reallocMemory(void *abc, size_t elem_size, int num_elem):&#160;utils.cpp'],['../utils_8h.html#a87f80403a65ac3b4bc755296834deb25',1,'reallocMemory(void *, size_t, int num_elem):&#160;utils.cpp']]],
  ['reset',['reset',['../classFeature.html#ad9ad5825f279361cbed4b75aa80e9a7e',1,'Feature']]],
  ['restorennet',['restoreNNet',['../classNNNer.html#a2589229bc5c4b421b03f321889dfc451',1,'NNNer']]]
];
