var searchData=
[
  ['n_5fkeys',['n_keys',['../classHash.html#abf3829bad61bbe6a800a0ef804d7b021',1,'Hash']]],
  ['ner',['ner',['../nnner__server_8cpp.html#a6f41e167ecb58c8b2db867c504d88ac9',1,'nnner_server.cpp']]],
  ['ner_5fhash',['ner_hash',['../classTokenizer.html#a335827eaa06d72e96d526bbc5b160b55',1,'Tokenizer::ner_hash()'],['../nnner__server_8cpp.html#a600056e590f6fe64f0f6105876406e58',1,'ner_hash():&#160;nnner_server.cpp']]],
  ['npmi_5fhash',['npmi_hash',['../classTokenizer.html#aa50b8398c3985fcabec8c491cb5fdbf8',1,'Tokenizer::npmi_hash()'],['../nnner__server_8cpp.html#a76fe08ae56a01c53cb6e7b68c9318cc8',1,'npmi_hash():&#160;nnner_server.cpp']]],
  ['npmi_5fidx',['npmi_idx',['../classTokens.html#a0c8081fcc64914bbc8560676eabf7590',1,'Tokens']]],
  ['npmifeature',['npmiFeature',['../classNNNer.html#a8ced6fb5b07effd9e5d5255a793bd94e',1,'NNNer::npmiFeature()'],['../nnner__server_8cpp.html#ac53df1d6d208f622ac92748b11663a8f',1,'npmiFeature():&#160;nnner_server.cpp']]],
  ['num_5fwords',['num_words',['../classTokens.html#a34ec7ea49872ae8ac2dcc9b243a0265b',1,'Tokens']]]
];
