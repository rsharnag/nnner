var searchData=
[
  ['test',['test',['../classNNNer.html#a5da2c70f532de074e71ddd21a01078be',1,'NNNer::test(vector&lt; Tokens &gt; &amp;training_data)'],['../classNNNer.html#a20e86f2c2ebfb5bb2eb58046fa30e679',1,'NNNer::test(Tokens &amp;training_data)']]],
  ['testner',['testNER',['../nnner__server_8cpp.html#a752f7771d2ea2d322da91feb3541bd12',1,'nnner_server.cpp']]],
  ['tokenize',['tokenize',['../classTokenizer.html#a7301f67905e29eac4f6ed16e43de6575',1,'Tokenizer']]],
  ['tokenize_5fcolumn_5fformat',['tokenize_column_format',['../classTokenizer.html#a6ac81be31af3086b3a70dd587720a387',1,'Tokenizer']]],
  ['tokenize_5fsent_5fformat',['tokenize_sent_format',['../classTokenizer.html#a3d6e2912f2a8bad2ed7ac7aa27bc9693',1,'Tokenizer']]],
  ['tokenizer',['Tokenizer',['../classTokenizer.html#a0b18c9402cc0b8a074e949cc313a7335',1,'Tokenizer']]],
  ['tokens',['Tokens',['../classTokens.html#a12cc0c7a796c2065d24873485052ae5c',1,'Tokens::Tokens()'],['../classTokens.html#ae46072f6e8c3a4326972ec075aaafcca',1,'Tokens::Tokens(int numTokens)'],['../classTokens.html#a3539fbd481db197dc0049837896c5b2e',1,'Tokens::Tokens(const Tokens &amp;tok)']]],
  ['trimwhitespace',['trimwhitespace',['../Tokenizer_8cpp.html#abb65da82a3a825938944b47fa98321cb',1,'Tokenizer.cpp']]]
];
