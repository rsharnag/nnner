var searchData=
[
  ['l1_5fbias',['l1_bias',['../classNNNer.html#a9685b4ba15bcf35b5278465b0c51dc2a',1,'NNNer']]],
  ['l1_5fweight',['l1_weight',['../classNNNer.html#aace4195063d84b2e0c69c41df1294842',1,'NNNer']]],
  ['l2_5fbias',['l2_bias',['../classNNNer.html#ac763dfaac0371a9c25201d22efbbf799',1,'NNNer']]],
  ['l2_5fweight',['l2_weight',['../classNNNer.html#a0568eb5ca818d892e4a4415cf94d6a55',1,'NNNer']]],
  ['labels',['labels',['../classNNNer.html#a93433e4309d34543295e658bbf2482bb',1,'NNNer']]],
  ['lookup_5ftable',['lookup_table',['../classFeature.html#a6c08ee094400aa3895f3aeed2894e01c',1,'Feature']]],
  ['lr',['lr',['../classNNNer.html#a798652673ccdf8e932852bfb60861f2d',1,'NNNer']]]
];
