var searchData=
[
  ['m_5fcfg',['m_cfg',['../classNERConfiguration.html#aca597603bb1233f6370a5600f3d5c9d7',1,'NERConfiguration']]],
  ['m_5fscope',['m_scope',['../classNERConfiguration.html#a11dc03199dbddec2a2ef24017ecd3751',1,'NERConfiguration']]],
  ['m_5fstr',['m_str',['../classNERConfigurationException.html#a644880eade661a5339fbb7b52f1f7bab',1,'NERConfigurationException']]],
  ['max_5fepoch',['max_epoch',['../classNNNer.html#a66e0cdd783a1c25515a0f30bf8424061',1,'NNNer']]],
  ['max_5ftags',['max_tags',['../classTokenizer.html#a5626024aa90e2d2e84f96df2187814c3',1,'Tokenizer']]],
  ['max_5ftokens',['max_tokens',['../classTokenizer.html#ad3ee4a906fde6ba1ffd62cccc82e3cd7',1,'Tokenizer']]]
];
