var searchData=
[
  ['idxc',['IDXC',['../utils_8h.html#a67de4baf747a35aa31ca9e9595de2b4a',1,'utils.h']]],
  ['idxr',['IDXR',['../utils_8h.html#a4376a351954d39ff4394785efeb3753a',1,'utils.h']]],
  ['info',['INFO',['../utils_8h.html#ae1103fea1e1b3c41ca3322d5389f7162',1,'utils.h']]],
  ['initner',['initNER',['../nnner__server_8cpp.html#ac17b45e742286862982ca505f84eea4e',1,'nnner_server.cpp']]],
  ['initnet',['initNet',['../classNNNer.html#ae9ef0b321ab3d8e3cbc7652407f0e1d1',1,'NNNer']]],
  ['input_5fstate',['input_state',['../classNNNer.html#aaccb5698a2bc63224322f7e3430e1328',1,'NNNer']]],
  ['input_5fstate_5fsize',['input_state_size',['../classNNNer.html#af6acaf3cd62267fb12f71c71ea21e2dd',1,'NNNer']]],
  ['is_5flittle_5fendian_5fcpu',['is_little_endian_cpu',['../utils_8cpp.html#abfb3db15dc37b21dd9933fabe85481ad',1,'utils.cpp']]],
  ['isprob',['ISPROB',['../nnlib_8cpp.html#aa00379e60f687ec1cccd623307b9e733',1,'nnlib.cpp']]]
];
