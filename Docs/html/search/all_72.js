var searchData=
[
  ['rand',['rand',['../utils_8cpp.html#ae89ecdb4663673e009c78ccd095a667d',1,'rand(size_t m, size_t n):&#160;utils.cpp'],['../utils_8h.html#ae89ecdb4663673e009c78ccd095a667d',1,'rand(size_t m, size_t n):&#160;utils.cpp']]],
  ['randn',['randn',['../utils_8cpp.html#a50980f2f035c2dd59543cdc39a68cea2',1,'randn(size_t m, size_t n):&#160;utils.cpp'],['../utils_8h.html#a50980f2f035c2dd59543cdc39a68cea2',1,'randn(size_t m, size_t n):&#160;utils.cpp']]],
  ['random',['random',['../classNNNer.html#a238d46a1f1ee5dd0b2ee89453b74578f',1,'NNNer']]],
  ['randrange',['randRange',['../utils_8cpp.html#a91cfbd41b83abd9763d8293e37300629',1,'randRange(size_t m, size_t n, real a, real b):&#160;utils.cpp'],['../utils_8h.html#a91cfbd41b83abd9763d8293e37300629',1,'randRange(size_t m, size_t n, real a, real b):&#160;utils.cpp']]],
  ['read_5fvector_5f1d',['read_vector_1d',['../utils_8cpp.html#a56d6f9c9dcb527731320aba3056e2630',1,'read_vector_1d(real **vec, int *vecSize, FILE *f, bool binary):&#160;utils.cpp'],['../utils_8h.html#a4480eeabd59b30642a9009025f0e3c32',1,'read_vector_1d(real **vec, int *row, int *col, FILE *f, bool binary):&#160;utils.h']]],
  ['read_5fvector_5f2d',['read_vector_2d',['../utils_8cpp.html#ac53d28572029e531797680dc9d250b16',1,'read_vector_2d(real **vec, int *row, int *col, FILE *f, bool binary):&#160;utils.cpp'],['../utils_8h.html#ac53d28572029e531797680dc9d250b16',1,'read_vector_2d(real **vec, int *row, int *col, FILE *f, bool binary):&#160;utils.cpp']]],
  ['real',['real',['../nnlib_8h.html#a11d147c64891830c9e79b3315b1b2e21',1,'real():&#160;nnlib.h'],['../utils_8h.html#a11d147c64891830c9e79b3315b1b2e21',1,'real():&#160;utils.h']]],
  ['reallocmemory',['reallocMemory',['../utils_8cpp.html#a3fa303c494e2499ac5c03d2cf5f190b5',1,'reallocMemory(void *abc, size_t elem_size, int num_elem):&#160;utils.cpp'],['../utils_8h.html#a87f80403a65ac3b4bc755296834deb25',1,'reallocMemory(void *, size_t, int num_elem):&#160;utils.cpp']]],
  ['reset',['reset',['../classFeature.html#ad9ad5825f279361cbed4b75aa80e9a7e',1,'Feature']]],
  ['residf_5fhash',['residf_hash',['../classTokenizer.html#a551c0a74ecff9d4d5c944f3f3e78be94',1,'Tokenizer::residf_hash()'],['../nnner__server_8cpp.html#ae4934ead00b4e3454c121b1a1e515c51',1,'residf_hash():&#160;nnner_server.cpp']]],
  ['residf_5fidx',['residf_idx',['../classTokens.html#af69253b09cf7df71948c18dab663df0a',1,'Tokens']]],
  ['residffeature',['resIdfFeature',['../classNNNer.html#a264ed4c75c68dbcbe2564984bda3f9c7',1,'NNNer::resIdfFeature()'],['../nnner__server_8cpp.html#a98e1e2e526acc51d5c940ff055d14246',1,'residfFeature():&#160;nnner_server.cpp']]],
  ['resp',['resp',['../nnner__server_8cpp.html#a107279839a635808f001412f4873147b',1,'nnner_server.cpp']]],
  ['restorennet',['restoreNNet',['../classNNNer.html#a2589229bc5c4b421b03f321889dfc451',1,'NNNer']]]
];
