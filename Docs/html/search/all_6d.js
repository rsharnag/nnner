var searchData=
[
  ['m_5fcfg',['m_cfg',['../classNERConfiguration.html#aca597603bb1233f6370a5600f3d5c9d7',1,'NERConfiguration']]],
  ['m_5fscope',['m_scope',['../classNERConfiguration.html#a11dc03199dbddec2a2ef24017ecd3751',1,'NERConfiguration']]],
  ['m_5fstr',['m_str',['../classNERConfigurationException.html#a644880eade661a5339fbb7b52f1f7bab',1,'NERConfigurationException']]],
  ['main',['main',['../nnner_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;nnner.cpp'],['../nnner__server_8cpp.html#a3c04138a5bfe5d72780bb7e82a18e627',1,'main(int argc, char **argv):&#160;nnner_server.cpp']]],
  ['max_5fepoch',['max_epoch',['../classNNNer.html#a66e0cdd783a1c25515a0f30bf8424061',1,'NNNer']]],
  ['max_5fsentence_5fsize',['MAX_SENTENCE_SIZE',['../nnner__server_8cpp.html#afec39886a23aa46ba316e65763b64020',1,'nnner_server.cpp']]],
  ['max_5fstring',['MAX_STRING',['../Hash_8h.html#ab5187269936538ffb8ccbbe7115ffdbc',1,'Hash.h']]],
  ['max_5ftags',['max_tags',['../classTokenizer.html#a5626024aa90e2d2e84f96df2187814c3',1,'Tokenizer']]],
  ['max_5ftokens',['max_tokens',['../classTokenizer.html#ad3ee4a906fde6ba1ffd62cccc82e3cd7',1,'Tokenizer']]],
  ['maxlabels',['maxlabels',['../nnlib_8cpp.html#a282490e07938e87ed7753e348b67f9d6',1,'nnlib.cpp']]],
  ['maxline',['MAXLINE',['../Tokenizer_8h.html#a3e937c42922f7601edb17b747602c471',1,'Tokenizer.h']]],
  ['maxstring',['MAXSTRING',['../Tokenizer_8h.html#a2ff420a6c6a2d1effc47a81e37327985',1,'Tokenizer.h']]],
  ['msg',['MSG',['../utils_8h.html#a0c719c414608ef14852670b063876c07',1,'utils.h']]]
];
