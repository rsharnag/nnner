var searchData=
[
  ['paramfile',['paramFile',['../classNNNer.html#a8f48e0145887fa87b6de6740c3929527',1,'NNNer']]],
  ['port',['PORT',['../nnner__server_8cpp.html#ad4ec83019a47110d8bee7563e0f4bcc4',1,'nnner_server.cpp']]],
  ['prev_5fcost',['prev_cost',['../classNNNer.html#a8f27d5c43dbf98dad1b2ce9a15e5fc58',1,'NNNer']]],
  ['prev_5fl1_5fbias',['prev_l1_bias',['../classNNNer.html#ad4301e8525fa1f52c4b290b05cce44ed',1,'NNNer']]],
  ['prev_5fl1_5fweight',['prev_l1_weight',['../classNNNer.html#a9b90290e9b391e7e73b08d95e94a4370',1,'NNNer']]],
  ['prev_5fl2_5fbias',['prev_l2_bias',['../classNNNer.html#a82d3f97604100b18669ea61b13bfc175',1,'NNNer']]],
  ['prev_5fl2_5fweight',['prev_l2_weight',['../classNNNer.html#aa9149f937a872a4a2bbbbdb9b912cf5c',1,'NNNer']]]
];
