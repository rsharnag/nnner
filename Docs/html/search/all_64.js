var searchData=
[
  ['debug_5flevel',['DEBUG_LEVEL',['../utils_8cpp.html#a8b89eef672a83a1a0b66ef1ba6b10b48',1,'utils.cpp']]],
  ['deserialize',['deserialize',['../classFeature.html#a0366fc64ab69069a28180e9817c167f1',1,'Feature']]],
  ['dice_5fhash',['dice_hash',['../classTokenizer.html#acfce64ee1bc0f5f3bd5b9e7a460e1fb6',1,'Tokenizer::dice_hash()'],['../nnner__server_8cpp.html#a618aff5ebff75aa5e0354cbc0b5062d5',1,'dice_hash():&#160;nnner_server.cpp']]],
  ['dice_5fidx',['dice_idx',['../classTokens.html#a3fe25878f10988b6cc785e9e819c1a00',1,'Tokens']]],
  ['dicefeature',['diceFeature',['../classNNNer.html#a10f91c253571b0de6b289cc09ce8cdf3',1,'NNNer::diceFeature()'],['../nnner__server_8cpp.html#a35fa469d52df7f8bf99f97d71dd456fd',1,'diceFeature():&#160;nnner_server.cpp']]],
  ['dsigmoid',['dsigmoid',['../classNNNer.html#a3b109399d75e1f406b7e06b537a7b5f0',1,'NNNer']]],
  ['dsoftmax',['dsoftmax',['../classNNNer.html#aeffa249f0baa572d1f27bb17d541b0f2',1,'NNNer']]],
  ['dtanh',['dtanh',['../classNNNer.html#a953b9b6ee803ae7f6de634aca2383256',1,'NNNer']]]
];
