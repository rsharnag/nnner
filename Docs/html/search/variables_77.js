var searchData=
[
  ['window_5fsize',['window_size',['../classNNNer.html#a3c9db3175aa30a127a5e262f47a74d2f',1,'NNNer']]],
  ['word_5fhash',['word_hash',['../classTokenizer.html#aa87d1ff9d133756c9e8de4b65a472b72',1,'Tokenizer::word_hash()'],['../nnner__server_8cpp.html#ab00b9090733431c42802e0e7a7940cea',1,'word_hash():&#160;nnner_server.cpp']]],
  ['word_5fidx',['word_idx',['../classTokens.html#a91cdce407196f367c80223bbd9728949',1,'Tokens']]],
  ['word_5flookup_5ftable',['word_lookup_table',['../classNNNer.html#af2fa1ff50429b3aba44f11438b2ef64c',1,'NNNer']]],
  ['word_5fpadding_5fidx',['word_padding_idx',['../classNNNer.html#aa36dd0cdc0fbed06620f7cd4ae7a0474',1,'NNNer']]],
  ['words',['words',['../classTokens.html#a58e1d4e230717364e16ba4a4151abdbc',1,'Tokens']]],
  ['wordvecsize',['wordVecSize',['../classNNNer.html#a5bf010c5802ce0192ea84a67c9a751cb',1,'NNNer']]]
];
