/*
 * Tokenizer.h
 *
 *  Created on: 16-May-2014
 *      Author: cfilt
 */

#ifndef TOKENIZER_H_
#define TOKENIZER_H_
#include <stdio.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "utils.h"
#include "Hash.h"
using namespace std;
#define MAXLINE 16000
#define MAXSTRING MAX_STRING
/**
 * Tokens class stores sentence in  a tokenized format. It captures all the features related each word in the sentence.
 */
class Tokens {
public:
	char** words; ///< array of string of words
	char*** tags; ///< array of string of tags
	int* output_tag_idx; ///< output tag index
	int* sys_tag_idx; ///< system generated tag index
	int tag_count; ///< tag count
	int* word_idx; ///< word index in vocabulary
	int num_words; ///< number of words in a sentence
	//int output_vector_size;
	/* feature vector*/
	Hash* tagHash; ///< pointer to tag hash
	int* cap_feature; ///< index vector  to capitalization features
	int* gazp_idx; 	///< index vector  to person gazetteer  features
	int* gazo_idx;	///< index vector  to organization gazetteer features
	int* gazl_idx;	///< index vector  to location gazetteer features
	int* gazm_idx;	///< index vector  to misc gazetteer features
	int* bdll_idx;	///< index vector  to bdll features
	int* dice_idx;	///< index vector  to dice features
	int* entropy_idx;	///< index vector  to entropy features
	int* npmi_idx;	///< index vector  to npmi features
	int* residf_idx;	///< index vector  to residf features

	Tokens() :
			num_words(0), words(NULL), tags(NULL), word_idx(NULL), output_tag_idx(
					NULL), tag_count(0), sys_tag_idx(
					NULL),tagHash(NULL),cap_feature(NULL) { ///< Null constructor
			 gazp_idx=NULL;
			 gazo_idx=NULL;
			 gazl_idx=NULL;
			 gazm_idx=NULL;
			 bdll_idx=NULL;
			 dice_idx=NULL;
			 entropy_idx=NULL;
			 npmi_idx=NULL;
			 residf_idx=NULL;
	}
	Tokens(int numTokens){ ///< Token size constructor
		num_words=numTokens;
		words = (char**) allocMemory(sizeof(char*), num_words);
		tags = (char***) allocMemory(sizeof(char**), num_words);
		word_idx = (int *) allocMemory(sizeof(int), num_words);
		output_tag_idx = (int*) allocMemory(sizeof(int), num_words);
		gazp_idx=(int *) allocMemory(sizeof(int), num_words);
		gazo_idx=(int *) allocMemory(sizeof(int), num_words);
		gazl_idx=(int *) allocMemory(sizeof(int), num_words);
		gazm_idx=(int *) allocMemory(sizeof(int), num_words);
		bdll_idx=(int *) allocMemory(sizeof(int), num_words);
		dice_idx=(int *) allocMemory(sizeof(int), num_words);
		entropy_idx=(int *) allocMemory(sizeof(int), num_words);
		npmi_idx=(int *) allocMemory(sizeof(int), num_words);
		residf_idx=(int *) allocMemory(sizeof(int), num_words);
		cap_feature = (int*) allocMemory(sizeof(int),num_words);
		sys_tag_idx = (int*) allocMemory(sizeof(int), num_words);
		tag_count=0;
		tagHash = NULL;


	}
	/**
	 * Copy constructor
	 */
	Tokens(const Tokens& tok) {
		tagHash=tok.tagHash;
		num_words = tok.num_words;
		words = (char**) allocMemory(sizeof(char*), tok.num_words);
		tags = (char***) allocMemory(sizeof(char**), tok.num_words);
		word_idx = (int *) allocMemory(sizeof(int), tok.num_words);
		output_tag_idx = (int*) allocMemory(sizeof(int), tok.num_words);
		if(tok.gazp_idx)gazp_idx=(int *) allocMemory(sizeof(int), tok.num_words); else gazp_idx=NULL;
		if(tok.gazo_idx)gazo_idx=(int *) allocMemory(sizeof(int), tok.num_words); else gazo_idx=NULL;
		if(tok.gazl_idx)gazl_idx=(int *) allocMemory(sizeof(int), tok.num_words); else gazl_idx=NULL;
		if(tok.gazm_idx) gazm_idx=(int *) allocMemory(sizeof(int), tok.num_words); else gazm_idx=NULL;
		if(tok.bdll_idx)bdll_idx=(int *) allocMemory(sizeof(int), tok.num_words); else bdll_idx=NULL;
		if(tok.dice_idx)dice_idx=(int *) allocMemory(sizeof(int), tok.num_words); else dice_idx=NULL;
		if(tok.entropy_idx)entropy_idx=(int *) allocMemory(sizeof(int), tok.num_words); else entropy_idx=NULL;
		if(tok.npmi_idx)npmi_idx=(int *) allocMemory(sizeof(int), tok.num_words); else npmi_idx=NULL;
		if(tok.residf_idx)residf_idx=(int *) allocMemory(sizeof(int), tok.num_words); else residf_idx=NULL;
		if(tok.cap_feature)cap_feature = (int*) allocMemory(sizeof(int), tok.num_words);
		else cap_feature= NULL;
		sys_tag_idx = (int*) allocMemory(sizeof(int), tok.num_words);
		tag_count = tok.tag_count;
		for (int i = 0; i < num_words; i++) {
			words[i] = (char*) allocMemory(sizeof(char),
					strlen(tok.words[i]) + 1);
			memcpy(words[i], tok.words[i],
					sizeof(char) * (strlen(tok.words[i]) + 1));
			tags[i] = (char**) allocMemory(sizeof(char *), tag_count);
			for (int j = 0; j < tag_count; j++) {
				tags[i][j] = (char *) allocMemory(sizeof(char),
						strlen(tok.tags[i][j]) + 1);
				memcpy(tags[i][j], tok.tags[i][j],
						sizeof(char) * (strlen(tok.tags[i][j]) + 1));
			}
		}
		memcpy(word_idx, tok.word_idx, sizeof(int) * num_words);
		memcpy(output_tag_idx, tok.output_tag_idx, sizeof(int) * num_words);
		if(tok.gazp_idx) memcpy(gazp_idx,tok.gazp_idx,sizeof(int) * num_words);
		if(tok.gazo_idx) memcpy(gazo_idx,tok.gazo_idx,sizeof(int) * num_words);
		if(tok.gazl_idx) memcpy(gazl_idx,tok.gazl_idx,sizeof(int) * num_words);
		if(tok.gazm_idx) memcpy(gazm_idx,tok.gazm_idx,sizeof(int) * num_words);
		if(tok.bdll_idx) memcpy(bdll_idx,tok.bdll_idx,sizeof(int) * num_words);
		if(tok.dice_idx) memcpy(dice_idx,tok.dice_idx,sizeof(int) * num_words);
		if(tok.entropy_idx) memcpy(entropy_idx,tok.entropy_idx,sizeof(int) * num_words);
		if(tok.npmi_idx) memcpy(npmi_idx,tok.npmi_idx,sizeof(int) * num_words);
		if(tok.residf_idx) memcpy(residf_idx,tok.residf_idx,sizeof(int) * num_words);
		if(tok.cap_feature) memcpy(cap_feature,tok.cap_feature,sizeof(int)*num_words);
		if (tok.sys_tag_idx) {
			memcpy(sys_tag_idx, tok.sys_tag_idx, sizeof(int) * num_words);
		} else {
			free(sys_tag_idx);
			sys_tag_idx = NULL;
		}
		//output_vector_size = tok.output_vector_size;

	}
	/**
	 * Destructor
	 */
	virtual ~Tokens() {
		for (int i = 0; i < num_words; i++) {
			free(words[i]);
			for (int j = 0; j < tag_count; j++) {
				free(tags[i][j]);
			}
			free(tags[i]);
		}
		free(words);
		free(tags);
		free(output_tag_idx);
		free(sys_tag_idx);
		free(word_idx);
		free(cap_feature);
		free(gazp_idx);
		free(gazo_idx);
		free(gazl_idx);
		free(gazm_idx);
		free(bdll_idx);
		free(dice_idx);
		free(entropy_idx);
		free(npmi_idx);
		free(residf_idx);

	}
	/**
	 * pretty print function
	 */
	void print() {
		for (int i = 0; i < num_words; i++)
			if(cap_feature)
				printf("%s\t%d\t%d\t%d\n", words[i], word_idx[i],cap_feature[i], output_tag_idx[i]);
			else
				printf("%s\t%d %d\n", words[i], word_idx[i], output_tag_idx[i]);
	}
	/**
	 * Setter for tag hash
	 * @param hash - tag hash
	 */
	void setTagHash(Hash* hash){
		this->tagHash=hash;
	}
	/**
	 * Writes the data to a output file
	 */
	void writeData(FILE* f) {
		for (int i = 0; i < num_words; i++) {
			fprintf(f, "%s\t", words[i]);
			for (int k = 0; k < tag_count; k++)
				fprintf(f, "%s\t", tags[i][k]);
			if(!tagHash) print_error("Set tag tagHash first");
			fprintf(f, "%s\n", tagHash->getKey(sys_tag_idx[i]));
		}
	}
	/**
	 * Pretty print with comparitive ouput of the system and gold data
	 */
	void pretty_print(){
		for (int i = 0; i < num_words; i++) {
			printf("%s_", words[i]);
			if(!tagHash) print_error("Set tag tagHash first");
				printf( "%s ", tagHash->getKey(sys_tag_idx[i]));
		}
		printf("\b\n");
		fflush(stdout);
	}
	/**
	 * Pretty write with comparitive ouput of the system and gold data
	 */
	void pretty_write(string& s){
		s="";
		for (int i = 0; i < num_words; i++) {
			s+= string(words[i])+"_";
			if(!tagHash) print_error("Set tag tagHash first");
			s+= string(tagHash->getKey(sys_tag_idx[i]))+" ";
		}
		s+="\n";
	}

};
/**
 * Tokenizer is class that encapsulates various methods to tokenize a input file. Currently
 * methods for column format tokenization and sentence tokenization exists.
 */
class Tokenizer {
private:
	int max_tokens; ///< max tokens
	int max_tags;   ///< max tags
	Hash* word_hash; ///< word hash
	Hash* ner_hash; ///< ner tag hash
	Hash* cap_hash;///< cap hash
	Hash* gazt_hash; ///< gazetteer value hash
	Hash* gazp_hash;///< person gazetteer hash
	Hash* gazo_hash; ///< organization gazetteer hash
	Hash* gazm_hash; ///< misc gazetteer hash
	Hash* gazl_hash; ///< location gazetteer hash
	Hash* bdll_hash; ///< bdll gazetteer hash
	Hash* dice_hash; ///< dice gazetteer hash
	Hash* entropy_hash; ///< entropy gazetteer hash
	Hash* npmi_hash; ///< npmi hash
	Hash* residf_hash; ///< residual idf hash
	/* feature property*/
	int checkCap(const char* w ,int size);
public:
	/**
	 * Constructor
	 * @param word_hash - hash of words in vocabulary
	 * @param ner_hash - hash of ner tags
	 */
	Tokenizer(Hash* word_hash, Hash* ner_hash) :
			word_hash(word_hash), ner_hash(ner_hash), max_tokens(50), max_tags(
					5) {
		cap_hash=NULL;
		gazt_hash=NULL;
		gazp_hash=NULL;
		gazl_hash=NULL;
		gazo_hash=NULL;
		gazm_hash=NULL;
		bdll_hash=NULL;
		dice_hash=NULL;
		entropy_hash=NULL;
		npmi_hash=NULL;
		residf_hash=NULL;
	}
	/**
	 * Destructor
	 */
	virtual ~Tokenizer();
	/**
	 * Tokenizes a  column format file in to \p Tokens
	 * @param filename - path to the file
	 * @param toks - vector of Tokens to be populated
	 */
	void tokenize_column_format(const char* filename, vector<Tokens>&toks);
	/**
	 * Sentence tokenizer. This doesnt populate the target tags\
	 * @param sent - sentence to be tokenized
	 * @return tokenized sentence
	 */
	Tokens tokenize_sent_format(string& sent);
	/**
	 * I dont remember what i was doing here
	 * \todo For future implementation of some other tokenization format
	 */
	void tokenize(vector<Tokens>& toks);
	/**
	 * Tokenizes the gazetteers. Looks up the gazetteer list and sets up the appropriate index for the gazetteer.
	 * @param toks - tokens to be looked up in gazetteer
	 * @param tokens - Tokens to be looked up
	 * @param gaz_hash - Gazetteer hash
	 * @param  gazt_hash - Gazetteer value hash (YES/NO)
	 * @param tag_id - index value to be returned
	 * \todo - implementation needs refinement
	 */
	void gazetteer_tokenizer(Tokens& toks,char** tokens,Hash* gaz_hash,Hash* gazt_hash,int* tag_id);

	/**
	 * Splits words using symbols as delimiter
	 * @param s - word to split
	 * @return split of words in a vector
	 */
	string breakWord(string s);

	const Hash* getCapHash() const { ///< Cap hash getter
		return cap_hash;
	}

	void setCapHash(Hash* capHash) {///< Cap hash setter
		cap_hash = capHash;
	}

	const Hash* getGazlHash() const {///< gazetteer location hash getter
		return gazl_hash;
	}

	void setGazlHash(Hash* gazlHash) {///< gazetteer location hash setter
		gazl_hash = gazlHash;
	}

	const Hash* getGazmHash() const {///< gazetteer misc  hash getter
		return gazm_hash;
	}

	void setGazmHash(Hash* gazmHash) {///< gazetteer misc  hash setter
		gazm_hash = gazmHash;
	}

	const Hash* getGazoHash() const {///< gazetteer organization hash getter
		return gazo_hash;
	}

	void setGazoHash(Hash* gazoHash) {///< gazetteer organization hash setter
		gazo_hash = gazoHash;
	}

	const Hash* getGazpHash() const {///< gazetteer person hash getter
		return gazp_hash;
	}

	void setGazpHash(Hash* gazpHash) {///< gazetteer person hash setter
		gazp_hash = gazpHash;
	}

	const Hash* getGaztHash() const {///< gazetteer type hash getter
		return gazt_hash;
	}

	void setGaztHash( Hash* gaztHash) {///< gazetteer type hash setter
		gazt_hash = gaztHash;
	}

	void setBdllHash( Hash*& bdllHash) {///< BDLL hash setter
		bdll_hash = bdllHash;
	}

	void setDiceHash( Hash*& diceHash) {///< Dice hash setter
		dice_hash = diceHash;
	}

	void setEntropyHash( Hash*& entropyHash) {///< Entropy hash setter
		entropy_hash = entropyHash;
	}

	void setNpmiHash( Hash*& npmiHash) {///< NPMI hash setter
		npmi_hash = npmiHash;
	}

	void setResIdfHash(Hash*& residfHash) {///< ResidualIDF hash setter
		residf_hash = residfHash;
	}
};

#endif /* TOKENIZER_H_ */

