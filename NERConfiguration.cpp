/*
 * NERConfiguration.cpp
 *
 *  Created on: 29-May-2014
 *      Author: cfilt
 */

#include "NERConfiguration.h"
#include "config4cpp/Configuration.h"
using CONFIG4CPP_NAMESPACE::Configuration;
using CONFIG4CPP_NAMESPACE::ConfigurationException;
/**
 * Exception class implementation
 */
NERConfigurationException::NERConfigurationException(const char* str){
	m_str = new char[strlen(str)+1];
	strcpy(m_str,str);
}
/**
 * Copy Constructor
 */
NERConfigurationException::NERConfigurationException(const NERConfigurationException& other){
	m_str = new char[strlen(other.m_str)+1];
	strcpy(m_str,other.m_str);
}
/**
 * Destructor
 */
NERConfigurationException::~NERConfigurationException(){
	delete[] m_str;
}
const char* NERConfigurationException::c_str() const {
	return m_str;
}
/**
 * Configuration class
 *
 */
NERConfiguration::NERConfiguration() {
	m_cfg=Configuration::create();
	m_scope=0;

}
/**
 * Destructor
 */
NERConfiguration::~NERConfiguration() {
	delete[] m_scope;
	((Configuration*)m_cfg)->destroy();
}
/**
 * Integer lookup wrapper function
 * @param name - property name
 * @return Integer read
 */
int NERConfiguration::lookupInt(const char* name) const throw (NERConfigurationException){
	Configuration * cfg = (Configuration*) m_cfg;
		try{
			return cfg->lookupInt(m_scope,name);
		}catch (ConfigurationException& e) {
			throw NERConfigurationException(e.c_str());
		}
}
/**
 * Integer lookup wrapper function
 * @param name - property name
 * @return Integer read
 */
bool NERConfiguration:: lookupBoolean(const char* name) const throw(NERConfigurationException){
	Configuration * cfg = (Configuration*) m_cfg;
		try{
			return cfg->lookupBoolean(m_scope,name);
		}catch (ConfigurationException& e) {
			throw NERConfigurationException(e.c_str());
		}
}
/**
 * Parser wrapper for parsing the config file
 * @param cfgSource - configuration source file name
 * @param scope - Block to use from configuration file
 *
 */
void NERConfiguration::parse(const char* cfgSource,const char* scope)  throw (NERConfigurationException){
	Configuration* cfg= (Configuration*)m_cfg;
	m_scope = new char[strlen(scope)+1];
	strcpy(m_scope,scope);
	try{
		if(cfgSource!=0 && strcmp(cfgSource,"")!=0){
			cfg->parse(cfgSource);
		}
	}catch (const ConfigurationException& e) {
		throw NERConfigurationException(e.c_str());
	}
}
/**
 * Real value lookup wrapper function
 * @param name - property name
 * @return Double read
 */
real NERConfiguration::lookupReal(const char* name) const throw (NERConfigurationException){
	Configuration * cfg = (Configuration*) m_cfg;
	try{
		return cfg->lookupFloat(m_scope,name)*1.0;
	}catch (ConfigurationException& e) {
		throw NERConfigurationException(e.c_str());
	}
}
/**
 * String lookup wrapper function
 * @param name - property name
 * @return string read
 */
const char* NERConfiguration::lookupString(const char* name) const throw(NERConfigurationException){
	Configuration * cfg = (Configuration*) m_cfg;
	try{
		return cfg->lookupString(m_scope,name);
	}catch (ConfigurationException& e) {
		throw NERConfigurationException(e.c_str());
	}
}
